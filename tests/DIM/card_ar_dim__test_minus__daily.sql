{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_dim','test_dmt']) }}


with awm as 
    (
    select  
            ar.CARD_AR_CODE CARD_AR_NBR,
            ar.CARD_AR_ID CARD_AR_ID,
           -- '{{var("etl_date") }}'::timestamp TF_CREATED_AT,
           -- '2400-01-01' TF_UPDATED_AT,
            ar.SRC_STM SRC_STM,
            ar.UNQ_ID_IN_SRC_STM::int UNQ_ID_IN_SRC_STM,
            ar.EFF_TO_DT EFF_MAT_DT,
            ar.LC_ST LC_ST     
        from {{ ref('card_ar_smy') }} ar
        where to_date(ar.SNPST_DT,'YYYYMMDD') = '{{ var("etl_date") }}'::date
    ),
dmt as
    (
        select 
            CARD_AR_NBR,
            CARD_AR_ID,
            --TF_CREATED_AT,
            --TF_UPDATED_AT,
            SRC_STM,
            UNQ_ID_IN_SRC_STM,
            EFF_MAT_DT,
            LC_ST
        from {{ ref('card_ar_dim') }} carddim
         where '{{var("etl_date") }}'::date between carddim.tf_created_at and carddim.tf_updated_at-1
    ),
check_minus as
    (
        select 'awm'::varchar as source, awm.* from awm
            minus
        select 'awm'::varchar as source, dmt.* from dmt

        union all
        
        select 'dmt'::varchar as source, dmt.* from dmt
            minus
        select 'dmt'::varchar as source, awm.* from awm
    )
select * from check_minus