{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_dim','test_dmt'])}}

with count_aws_today as 
    (

 select 'br_dim'::varchar as source, count(*) as count_row
        from {{ ref('ip') }} ip
        join {{ ref('src_stm') }} src_stm on ip.SRC_STM_ID  = src_stm.SRC_STM_ID and src_stm.SRC_STM_CODE = 'T24_VPB_COMPANY'
        where ip.tf_created_at > '{{var("etl_date") }}'::timestamp 
                or ip.tf_deleted_at is null
    ),
count_dmt_today as
    (
        select 'br_dim'::varchar as source, count (*) as count_row
        from {{ ref('br_dim') }} br
        where '{{var("etl_date") }}'::timestamp between br.tf_created_at and br.tf_updated_at -1
    ),
check_count as
    (
        select
            {{dbt_date.today()}} ppn_dt,
            'Test_count'::varchar as Test_type,
            a.source as source_tbl,
            b.source as target_tbl,
            a.count_row as count_source,
            b.count_row as count_target
        from count_aws_today as a 
        full join count_dmt_today b on 1=1
    )

    select 
        *
    from check_count where count_source <> count_target