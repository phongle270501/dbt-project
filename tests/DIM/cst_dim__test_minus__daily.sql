{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_dim','test_dmt']) }}


with awm as 
    (
    select  ip.IP_ID cst_id,
                src_stm.SRC_STM_CODE src_stm,
                ip.UNQ_ID_IN_SRC_STM unq_id_in_src_stm,
                segment.SEG_CODE SEG_CODE,
                '2400-01-01'::timestamp tf_updated_at       
        from {{ ref('ip') }} ip
        join {{ ref('src_stm') }} src_stm on ip.SRC_STM_ID  = src_stm.SRC_STM_ID 
                        and src_stm.SRC_STM_CODE in  ('T24_CUSTOMER','WAY4_CLIENT')
        left join (select segment.cst_id, 
                            segment_value.CL_CODE SEG_CODE
                    from {{ ref('cl_x_cst') }} segment 
                    join  {{ ref('cl_val') }} cst_rltn_tp on segment.CL_X_CST_TP_ID = cst_rltn_tp.CL_ID
                                                             and cst_rltn_tp.CL_CODE ='T24-CST-TO-SEG'
                    join  {{ ref('cl_val') }} segment_value on segment.CL_ID = segment_value.CL_ID
                    where  '{{var("etl_date") }}'::date between segment.TF_CREATED_AT AND segment.TF_UPDATED_AT - 1
                     ) segment on ip.ip_id = segment.cst_id 
           where ip.tf_deleted_at > '{{var("etl_date") }}'::date 
                or ip.tf_deleted_at is null
    ),
dmt as
    (
        select 
            cst_id,
            src_stm,
            unq_id_in_src_stm,
            SEG_CODE,
            tf_updated_at
        from {{ ref('cst_dim') }} cst
        where '{{var("etl_date") }}'::date between cst.tf_created_at and cst.tf_updated_at -1
    ),

check_minus as
    (
        select 'awm'::varchar as source, awm.* from awm
            minus
        select 'awm'::varchar as source, dmt.* from dmt

        union all
        
        select 'dmt'::varchar as source, dmt.* from dmt
            minus
        select 'dmt'::varchar as source, awm.* from awm
    )
select * from check_minus