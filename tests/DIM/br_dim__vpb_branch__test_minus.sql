{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_dim','test_dmt'])}}


with src as 
    (
    select  {{ mcr_generate_surrogate_key([{'column': 'br.id'}, {'text': 'T24_VPB_COMPANY'} ]) }} as br_id,
                'T24_VPB_COMPANY' src_stm,
                br.id unq_id_in_src_stm,
                br_dtl.BRANCH_NAME br_nm     
        from {{ source('t24','t24_vpb_company') }} br
        join {{ source('t24','t24_vpb_company_details') }} br_dtl on br.ID = br_dtl.ID and br_dtl.M=1 and br_dtl.S=1 and br_dtl.act_dt = br.act_dt
        where br.act_dt = '{{var("etl_date") }}'::date
    ),
dmt as
    (
        select 
            br_id,
            src_stm,
            unq_id_in_src_stm,
            br_nm
        from {{ ref('br_dim') }} br
         where '{{var("etl_date") }}'::timestamp between br.tf_created_at and br.tf_updated_at-1
          and  src_stm  = 'T24_VPB_COMPANY'
      --  where row_num = 1
    ),
check_minus as
    (
        select 'src'::varchar as source, src.* from src
            minus
        select 'src'::varchar as source, dmt.* from dmt

        union all
        
        select 'dmt'::varchar as source, dmt.* from dmt
            minus
        select 'dmt'::varchar as source, src.* from src
    )
select * from check_minus