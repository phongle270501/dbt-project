{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_dim','test_dmt'])}}

with count_aws_today as 
    (
        select 
            'card_smy'::varchar as source
            ,count(*) as count_row
        from {{ ref('card_smy') }} pymtc
        where pymtc.snpst_dt = REPLACE('{{ var('etl_date') }}', '-', '')::int
    ),
count_dmt_today as
    (
        select
            'card_dim'::varchar as source
            ,count (*) as count_row
        from {{ ref('card_dim') }} cd
        where cd.tf_created_at <= '{{var("etl_date") }}'::date 
            and cd.tf_updated_at > '{{var("etl_date") }}'::date 
    ),
check_count as
    (
        select
            {{dbt_date.today()}} ppn_dt,
            'Test_count'::varchar as Test_type,
            a.source as source_tbl,
            b.source as target_tbl,
            a.count_row as count_source,
            b.count_row as count_target
        from count_aws_today as a
        full join count_dmt_today b on 1=1
    )
    select 
        *
    from check_count where count_source <> count_target
    union all
    select 
        *
    from check_count where count_target = 0