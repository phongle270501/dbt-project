{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_dim','test_dmt']) }}


with src as 
    (
    select  {{ mcr_generate_surrogate_key([{'column': 'cst.id'}, {'text':  'WAY4_CLIENT'} ]) }} as ip_id,
                'WAY4_CLIENT' src_stm,
                cst.id::varchar unq_id_in_src_stm,
                '' SEG_CODE       
        from {{ ref('stg_way4__client') }} cst
         where cst.act_dt = '{{var("etl_date") }}'::date
    ),
dmt as
    (
        select 
            cst_id,
            src_stm,
            unq_id_in_src_stm,
            coalesce (SEG_CODE,'') SEG_CODE
        from {{ ref('cst_dim') }} cst
        where '{{var("etl_date") }}'::date between cst.tf_created_at and cst.tf_updated_at -1
        and cst.src_stm = 'WAY4_CLIENT'
    ),
check_minus as
    (
        select 'src'::varchar as source, src.* from src
            minus
        select 'src'::varchar as source, dmt.* from dmt

        union all
        
        select 'dmt'::varchar as source, dmt.* from dmt
            minus
        select 'dmt'::varchar as source, src.* from src
    )
select * from check_minus