{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_dim','test_dmt'])}}

with count_aws_today as 
    (
 select 'card_ar_dim'::varchar as source, count(*) as count_row
        from {{ ref('card_ar_smy') }} ar
        where to_date(ar.SNPST_DT,'YYYYMMDD') = '{{ var("etl_date") }}'::date
    ),
count_dmt_today as
    (
        select 'card_ar_dim'::varchar as source, count (*) as count_row
        from {{ ref('card_ar_dim') }} carddim
        where '{{var("etl_date") }}'::timestamp between carddim.tf_created_at and carddim.tf_updated_at-1
    ),
check_count as
    (
        select
            {{dbt_date.today()}} ppn_dt,
            'Test_count'::varchar as Test_type,
            a.source as source_tbl,
            b.source as target_tbl,
            a.count_row as count_source,
            b.count_row as count_target
        from count_aws_today as a 
        full join count_dmt_today b on 1=1
    )

    select 
        *
    from check_count where count_source <> count_target