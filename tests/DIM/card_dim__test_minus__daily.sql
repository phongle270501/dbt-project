{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_dim','test_dmt'])}}


with awm as 
    (
        select
            pymtc.PYMTC_CODE as nbr
            ,pymtc.PYMTC_ID as card_id
            ,pymtc.EFF_FM_DT as eff_fm_dt
            ,pymtc.EFF_TO_DT as eff_to_dt
            ,pymtc.src_stm as src_stm
            ,pymtc.UNQ_ID_IN_SRC_STM as unq_id_in_src_stm
            ,pymtc.last_actvn_dt as last_actvn_dt
        from {{ ref('card_smy') }} pymtc
        where pymtc.snpst_dt = REPLACE('{{ var('etl_date') }}', '-', '')::int
    ),
dmt as
    (
        select 
            nbr
            ,card_id
            ,eff_fm_dt
            ,eff_to_dt
            ,src_stm
            ,unq_id_in_src_stm
            ,last_actvn_dt
        from {{ ref('card_dim') }} cd
        where cd.tf_created_at <= '{{var("etl_date") }}'::date 
            and cd.tf_updated_at > '{{var("etl_date") }}'::date 
    ),
check_minus as
    (
        select 'awm'::varchar as source, awm.* from awm
            minus
        select 'awm'::varchar as source, dmt.* from dmt

        union all
        
        select 'dmt'::varchar as source, dmt.* from dmt
            minus
        select 'dmt'::varchar as source, awm.* from awm
    )
select * from check_minus