{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_dim','test_dmt'])}}

        select 'CST_DIM' table_name, cst_dim_id DIM_ID, count(1) DL_SUP from {{ ref('cst_dim') }}
        group by cst_dim_id 
        having count(1)>1
        union 
         select 'BR_DIM' table_name, br_dim_id DIM_ID, count(1) DL_SUP from {{ ref('br_dim') }}
        group by br_dim_id 
        having count(1)>1 
         union      
       select 'CARD_AR_DIM' table_name, card_ar_dim_id DIM_ID, count(1) DL_SUP from {{ ref('card_ar_dim') }}
        group by card_ar_dim_id 
        having count(1)>1      
         union      
       select 'CARD_DIM' table_name, card_dim_id DIM_ID, count(1) DL_SUP from {{ ref('card_dim') }}
        group by card_dim_id 
        having count(1)>1      