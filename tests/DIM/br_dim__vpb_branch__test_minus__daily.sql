{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_dim','test_dmt'] ) }}


with awm as 
    (
    select  ip.IP_ID br_id,
                src_stm.SRC_STM_CODE src_stm,
                ip.UNQ_ID_IN_SRC_STM unq_id_in_src_stm,
                ip.IP_NM br_nm,
                ip.tf_created_at tf_created_at,
                '2400-01-01'::date tf_updated_at--,
             --   (case when ip.tf_deleted_at is not null then ip.tf_deleted_at else '2400-01-01'::date end)  tf_updated_at,
             --   '1'::int row_num         
        from {{ ref('ip') }} ip
        join {{ ref('src_stm') }} src_stm  on ip.SRC_STM_ID  = src_stm.SRC_STM_ID and src_stm.SRC_STM_CODE = 'T24_VPB_COMPANY'
        where ip.tf_created_at > '{{var("etl_date") }}'::date 
                or ip.tf_deleted_at is null
    ),
dmt as
    (
        select 
            br_id,
            src_stm,
            unq_id_in_src_stm,
            br_nm,
            tf_created_at,
            tf_updated_at--,
           -- row number () over (partition unq_id_in_src_stm order by tf_updated_at desc) row_num
        from {{ ref('br_dim') }} br
         where '{{var("etl_date") }}'::date between br.tf_created_at and br.tf_updated_at-1
   
      --  where row_num = 1
    ),
check_minus as
    (
        select 'awm'::varchar as source, awm.* from awm
            minus
        select 'awm'::varchar as source, dmt.* from dmt

        union all
        
        select 'dmt'::varchar as source, dmt.* from dmt
            minus
        select 'dmt'::varchar as source, awm.* from awm
    )
select * from check_minus