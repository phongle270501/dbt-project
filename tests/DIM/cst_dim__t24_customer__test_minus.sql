{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_dim','test_dmt']) }}


with src as 
    (
    select  {{ mcr_generate_surrogate_key([{'column': 'cst.customer_code'}, {'text': 'T24_CUSTOMER'} ]) }} as ip_id,
                'T24_CUSTOMER' src_stm,
                cst.customer_code unq_id_in_src_stm,
                cst.SEGMENT SEG_CODE       
        from {{ source('t24','t24_customer') }} cst
        join {{ source('t24','t24_customer_details') }} cst_dtl  on cst.customer_code = cst_dtl.customer_code and cst_dtl.M = '1' and cst_dtl.S = '1' and cst_dtl.act_dt = cst.act_dt 
        where cst.act_dt = '{{var("etl_date") }}'::date
    ),
dmt as
    (
        select 
            cst_id,
            src_stm,
            unq_id_in_src_stm,
            SEG_CODE
        from {{ ref('cst_dim') }} cst
        where '{{var("etl_date") }}'::date between cst.tf_created_at and cst.tf_updated_at -1
        and cst.src_stm = 'T24_CUSTOMER'
    ),
check_minus as
    (
        select 'src'::varchar as source, src.* from src
            minus
        select 'src'::varchar as source, dmt.* from dmt

        union all
        
        select 'dmt'::varchar as source, dmt.* from dmt
            minus
        select 'dmt'::varchar as source, src.* from src
    )
select * from check_minus