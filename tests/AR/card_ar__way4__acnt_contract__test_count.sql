{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_ar','test_awm'] 
            )}}

with count_stg as 
    (
        select 'stg_way4__acnt_contract'::varchar as source, count(*) as count_row
        from {{ ref('stg_way4__acnt_contract') }} acnt_ctr
        left join {{ ref('stg_way4__f_i') }} fi on acnt_ctr.f_i = fi.id
        where acnt_ctr.base_relation is null
    ),
count_awm as
    (
        select 'card_ar'::varchar as source, count (*) as count_row
        from {{ ref('card_ar') }}
        where SRC_STM_ID = {{ mcr_generate_surrogate_key([{'text': 'WAY4_ACNT_CONTRACT'} , {'text': 'CSV_SRC_STM'}]) }}
    ),
check_count as
    (
        select
            {{dbt_date.today()}} ppn_dt,
            'Test_count'::varchar as Test_type,
            a.source as source_tbl,
            b.source as target_tbl,
            a.count_row as count_source,
            b.count_row as count_target
        from count_stg a 
        full join count_awm b on 1=1
    )

    select 
        *
    from check_count where count_source <> count_target
        union
    select
        *
    from check_count where count_target = 0