{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_ar','test_awm'])}}

with stg as 
    (
        select 
            {{ mcr_generate_surrogate_key([{'column': 'acnt_ctr.ID'} , {'text': 'WAY4_ACNT_CONTRACT'}]) }} as card_ar_id
            ,{{ mcr_generate_surrogate_key([{'column': 'acnt_ctr.ID'} , {'text': 'W4-ACC-CTR-OUDE-DT'}, {'text': 'WAY4_ACNT_CONTRACT'}]) }} as card_ar_avy_id
            ,pd_date.pd_date as act_strt_dt
            ,{{ mcr_generate_surrogate_key([{'text': 'AR-AVY-TP'} , {'text': 'W4-ACC-CTR-ODUE-DT'} , {'text': 'CSV_CV'}]) }} as avy_tp_id
        from {{ ref('stg_way4__acnt_contract') }} acnt_ctr
            left join {{ ref('stg_way4__vpb_acnt_pd_date') }} pd_date on acnt_ctr.id = pd_date.ACNT_CONTRACT__OID
        where acnt_ctr.con_cat = 'C'
            and pd_date.pd_date is not null
    ),
awm as
    (
        select
            card_ar_id
            ,card_ar_avy_id
            ,act_strt_dt
            ,avy_tp_id
        from {{ ref('card_ar_avy') }}
        where avy_tp_id = {{ mcr_generate_surrogate_key([{'text': 'AR-AVY-TP'} , {'text': 'W4-ACC-CTR-ODUE-DT'}, {'text': 'CSV_CV'}]) }}
            and tf_created_at <= '{{var("etl_date") }}'::DATE
            and tf_updated_at > '{{var("etl_date") }}'::DATE
    ),
check_minus as
    (
        select 'stg'::varchar as source, stg.* from stg
            minus
        select 'stg'::varchar as source, awm.* from awm

        union all
        
        select 'awm'::varchar as source, awm.* from awm
            minus
        select 'awm'::varchar as source, stg.* from stg
    )
select * from check_minus