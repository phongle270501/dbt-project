{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_cl','test_awm'])}}

with stg as 
    (
        select 
            {{ mcr_generate_surrogate_key([{'column': 's.scm_code'}, {'text': 'CSV_CL_SCM'} ]) }} as cl_scm_id,
            {{ mcr_generate_surrogate_key([{'text': 'CSV_CL_SCM'}, {'text': 'CSV_SRC_STM'}]) }} as src_stm_id,
            s.scm_code as scm_code,
            s.shrt_nm as shrt_nm,
            '2400-01-01'::DATE  as tf_updated_at
        from {{ ref('stg_csv__csv_cl_scm') }} s
    ),
awm as
    (
        select
            cl_scm_id,
            src_stm_id,
            scm_code,
            shrt_nm,
            tf_updated_at
        from {{ ref('cl_scm') }} a
    ),
check_minus as
    (
        select 'stg'::varchar as source, stg.* from stg
            minus
        select 'stg'::varchar as source, awm.* from awm

        union all
        
        select 'awm'::varchar as source, awm.* from awm
            minus
        select 'awm'::varchar as source, stg.* from stg
    )

select * from check_minus