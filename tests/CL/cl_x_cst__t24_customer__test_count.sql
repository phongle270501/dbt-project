{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_cl','test_awm'])}}

with count_stg as 
    (
        select 'stg_t24__customer'::varchar as source, count(*) as count_row
        from {{ ref('stg_t24__customer') }} s
        where s.segment is not null
    ),
count_awm as
    (
        select 'cl_x_cst'::varchar as source, count (*) as count_row
        from {{ ref('cl_x_cst') }}
        where cl_x_cst_tp_id = {{ mcr_generate_surrogate_key([{'text': 'CL-X-IP-TP'}, {'text': 'T24-CST-TO-SEG'}, {'text': 'CSV_CV'} ]) }}
            and src_stm_id = {{ mcr_generate_surrogate_key([{'text': 'T24_CUSTOMER'}, {'text': 'CSV_SRC_STM'} ]) }}
            and tf_created_at <= '{{var("etl_date") }}'::DATE
            and tf_updated_at > '{{var("etl_date") }}'::DATE
    ),
check_count as
    (
        select
            {{dbt_date.today()}} ppn_dt,
            'Test_count'::varchar as Test_type,
            a.source as source_tbl,
            b.source as target_tbl,
            a.count_row as count_source,
            b.count_row as count_target
        from count_stg a 
        full join count_awm b on 1=1
    )
    select 
        *
    from check_count where count_source <> count_target