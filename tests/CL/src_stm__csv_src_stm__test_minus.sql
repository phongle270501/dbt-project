{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_cl','test_awm'])}}

with stg as 
    (
        select 
            s.eff_dt::date as eff_dt,
            s.end_dt::date as end_dt,
            s.prn_src_stm_id,
            s.src_stm_code,
            {{ mcr_generate_surrogate_key([{'column': 's.src_stm_code'}, {'text': 'CSV_SRC_STM'} ]) }} src_stm_id,
            s.src_stm_nm
        from {{ ref('stg_csv__csv_src_stm') }} s
    ),
awm as
    (
        select
            a.eff_dt,
            a.end_dt,
            a.prn_src_stm_id,
            a.src_stm_code,
            a.src_stm_id,
            a.src_stm_nm
        from {{ ref('src_stm') }} a
    ),
check_minus as
    (
        select 'stg'::varchar as source, stg.* from stg
            minus
        select 'stg'::varchar as source, awm.* from awm

        union all
        
        select 'awm'::varchar as source, awm.* from awm
            minus
        select 'awm'::varchar as source, stg.* from stg
    )

select * from check_minus