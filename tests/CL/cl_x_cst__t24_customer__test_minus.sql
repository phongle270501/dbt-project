{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_cl','test_awm'])}}

with stg as 
    (
        select
            {{ mcr_generate_surrogate_key([{'column': 't24_cst.customer_code'}, {'text': 'T24_CUSTOMER'} ]) }} as cst_id,
            {{ mcr_generate_surrogate_key([{'text': 'CL-X-IP-TP'}, {'text': 'T24-CST-TO-SEG'}, {'text': 'CSV_CV'} ]) }} as cl_x_cst_tp_id,
            {{ mcr_generate_surrogate_key([{'text': 'T24-191'}, {'text': 'CSV_CL_SCM'}]) }} as cl_scm_id,
            {{ mcr_generate_surrogate_key([{'text': 'T24-191'}, {'column': 't24_cst.segment'}, {'text': 'CSV_CV'} ]) }} as cl_id,
            {{ mcr_generate_surrogate_key([{'text': 'T24_CUSTOMER'}, {'text': 'CSV_SRC_STM'}]) }} as src_stm_id
        from {{ ref('stg_t24__customer') }} t24_cst
        where t24_cst.segment is not null
    ),
awm as
    (
        select 
            cst_id,
            cl_x_cst_tp_id,
            cl_scm_id,
            cl_id,
            src_stm_id
        from {{ ref('cl_x_cst') }}
        where cl_x_cst_tp_id = {{ mcr_generate_surrogate_key([{'text': 'CL-X-IP-TP'}, {'text': 'T24-CST-TO-SEG'}, {'text': 'CSV_CV'} ]) }}
            and src_stm_id = {{ mcr_generate_surrogate_key([{'text': 'T24_CUSTOMER'}, {'text': 'CSV_SRC_STM'} ]) }}
            and tf_created_at <= '{{var("etl_date") }}'::DATE
            and tf_updated_at > '{{var("etl_date") }}'::DATE
    ),
check_minus as
    (
        select 'stg'::varchar as source, stg.* from stg
            minus
        select 'stg'::varchar as source, awm.* from awm

        union all
        
        select 'awm'::varchar as source, awm.* from awm
            minus
        select 'awm'::varchar as source, stg.* from stg
    )
select * from check_minus