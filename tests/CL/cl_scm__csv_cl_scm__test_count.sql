{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_cl','test_awm'])}}

with count_stg as 
    (
        select 'stg_csv__cl_scm'::varchar as source, count(*) as count_row
        from {{ ref('stg_csv__csv_cl_scm') }} 
    ),
count_awm as
    (
        select 'cl_scm'::varchar as source, count (*) as count_row
        from {{ ref('cl_scm') }}
    ),
check_count as
    (
        select
            {{dbt_date.today()}} ppn_dt,
            'Test_count'::varchar as Test_type,
            a.source as source_tbl,
            b.source as target_tbl,
            a.count_row as count_source,
            b.count_row as count_target
        from count_stg a 
        full join count_awm b on 1=1
    )

    select 
        *
    from check_count where count_source <> count_target