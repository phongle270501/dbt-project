{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_smy','test_awm'] ) }}

with awm as 
    (
 select  
            ar.card_ar_id card_ar_id,
           '{{ var("etl_date") }}'::date snpst_dt,
            ar.CARD_AR_NM card_ar_code,
            ar.eff_dt eff_dt,
            ar.end_dt end_dt,
            source.src_stm_code src_stm,
            ar.UNQ_ID_IN_SRC_STM UNQ_ID_IN_SRC_STM,
            odue_dt.NBR_ODUE_DAY NBR_ODUE_DAY,
            stt.cl_code LC_ST,
            stt.cl_nm LC_ST_NM,
            coalesce(txn.amt_total,0::int) TOT_REPYMT_AMT_MTD, --0::int TOT_REPYMT_AMT_MTD
            ar.OPN_BR_ID OPN_BR_ID,
            branch.unq_id_in_src_stm OPN_BR_CODE,
            cst_t24.OBJ_IP_ID CST_ID,
            cst_t24.CST_CODE CST_CODE
        from {{ ref('card_ar') }} ar
        join {{ ref('cl_val') }} cv  on ar.ar_tp_id = cv.cl_id and cv.cl_code = 'W4-ACC-CTR'
        join {{ ref('src_stm') }} source  on ar.src_stm_id = source.src_stm_id
        join {{ ref('cl_val') }} stt  on  ar.CARD_LCS_TP_ID = stt.CL_ID     
        left join {{ ref('ip') }} branch  on  ar.OPN_BR_ID = branch.IP_ID
        left join (select  odue_dt.CARD_AR_ID card_ar_id,
                           DATEDIFF('DAY','{{ var("etl_date") }}'::date, odue_dt.ACT_STRT_DT)::int NBR_ODUE_DAY
                    from {{ ref('card_ar_avy') }} odue_dt  
                        join {{ ref('cl_val') }} odue_dt_tp on odue_dt.AVY_TP_ID = odue_dt_tp.CL_ID and  odue_dt_tp.CL_CODE ='W4-ACC-CTR-ODUE-DT'
                        where '{{ var("etl_date") }}'::timestamp between odue_dt.TF_CREATED_AT AND odue_dt.TF_UPDATED_AT -1
                    ) odue_dt on ar.CARD_AR_ID = odue_dt.CARD_AR_ID
 
        join (
            select  cst_t24.OBJ_IP_ID,
                    cst_t24.SBJ_IP_ID,
                    cst_merge.unq_id_in_src_stm CST_CODE
                from  {{ ref('ip_x_ip') }} 	cst_t24
                join {{ ref('cl_val') }} 	cst_t24_rltn_tp on  cst_t24.IP_X_IP_TP_ID = cst_t24_rltn_tp.CL_ID and cst_t24_rltn_tp.CL_CODE ='CST-TO-T24CIF'
                join {{ ref('ip') }} cst_merge on  cst_t24.OBJ_IP_ID = cst_merge.IP_ID
                where '{{ var("etl_date") }}'::date between cst_t24.TF_CREATED_AT AND cst_t24.TF_UPDATED_AT -1   
        ) cst_t24 on ar.CST_ID = cst_t24.SBJ_IP_ID
         left join (select TRGT_ANCHOR_ID,
                        sum_txn_amt amt_total 
                    from {{ source('deltalake','intr_dbk__card_txn_smy')}}  txn  where  
                     txn.snpst_dt =  '{{ var('etl_date') }}'::date
                                )

                 txn on ar.CARD_AR_ID = txn.TRGT_ANCHOR_ID 
),
smy as
    (
        select 
            s.CARD_AR_ID,
            to_date(s.SNPST_DT,'YYYYMMDD') SNPST_DT,
            s.CARD_AR_CODE,
            s.EFF_FM_DT,
            s.EFF_TO_DT,
            s.SRC_STM,
            s.UNQ_ID_IN_SRC_STM,
            s.NBR_ODUE_DAY,
            s.LC_ST,
            s.LC_ST_NM,
            coalesce(s.TOT_REPYMT_AMT_MTD,0::int) - coalesce(b.TOT_REPYMT_AMT_MTD,0::int)  TOT_REPYMT_AMT_MTD, --case when TOT_REPYMT_AMT_MTD is null then 0::int else TOT_REPYMT_AMT_MTD end,
            s.OPN_BR_ID,
            s.OPN_BR_CODE,
            s.CST_ID,
            s.CST_CODE    
        from {{ ref('card_ar_smy') }} s
        left join  {{ ref('card_ar_smy') }} b on s.CARD_AR_ID = b.CARD_AR_ID and b.SNPST_DT = REPLACE('{{ var('etl_date') }}', '-', '')::int - 1
        where s.SNPST_DT = REPLACE('{{ var('etl_date') }}', '-', '')::int
    ),
check_minus as
    (
        select 'awm'::varchar as source, awm.* from awm
            minus
        select 'awm'::varchar as source, smy.* from smy

        union all
        
        select 'smy'::varchar as source, smy.* from smy
            minus
        select 'smy'::varchar as source, awm.* from awm
    )
select * from check_minus