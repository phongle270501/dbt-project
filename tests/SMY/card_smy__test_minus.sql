{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_smy','test_awm'])}}


with lmt_amt as
    (
        select 
            lmt_amt.card_ar_id
            ,lmt_amt.avy_amt
        from  {{ ref('card_ar_avy') }} lmt_amt
            join {{ ref('cl_val') }} lmt_amt_tp on lmt_amt.avy_tp_id = lmt_amt_tp.cl_id 
                                    and lmt_amt_tp.cl_code ='W4-CARD-CTR-LMT-AMT'
        where
            lmt_amt.tf_created_at <= '{{var("etl_date") }}'::date
            and lmt_amt.tf_updated_at > '{{var("etl_date") }}'::date
    ),
unlock_dt as
    (
        select 
            unlock_dt.card_ar_id
            ,unlock_dt.ACT_STRT_DT
        from  {{ ref('card_ar_avy') }} unlock_dt
            join {{ ref('cl_val') }} unlock_dt_tp on unlock_dt.AVY_TP_ID = unlock_dt_tp.CL_ID 
                                    and unlock_dt_tp.CL_CODE ='W4-CARD-CTR-UNLOCK-DT'
        where
            unlock_dt.tf_created_at <= '{{var("etl_date") }}'::DATE
            and unlock_dt.tf_updated_at > '{{var("etl_date") }}'::DATE
    ),
cst_t24 as 
    (
        select 
            cst_t24.SBJ_IP_ID as SBJ_IP_ID
            ,cst_t24.OBJ_IP_ID as OBJ_IP_ID 
            ,cst_merge.UNQ_ID_IN_SRC_STM as unq_id_in_src_stm
        from {{ ref('ip_x_ip') }} cst_t24
            join {{ ref('cl_val') }} cst_t24_rltn_tp on cst_t24.IP_X_IP_TP_ID = cst_t24_rltn_tp.CL_ID
            left join {{ ref('ip') }} cst_merge on cst_t24.OBJ_IP_ID = cst_merge.IP_ID
        where cst_t24_rltn_tp.CL_CODE ='CST-TO-T24CIF'
            and cst_t24.tf_created_at <= '{{var("etl_date") }}'::DATE
            and cst_t24.tf_updated_at > '{{var("etl_date") }}'::DATE
    ),
awm as 
    (
        select 
            ar.card_ar_id::char(32) as pymtc_id
            ,REPLACE('{{ var('etl_date') }}', '-', '')::int as snpst_dt
            ,ar.prn_ar_id::char(32) card_ar_id
            ,ar.card_ar_nm::varchar(256) as pymtc_code
            ,prn_ar.card_ar_nm::varchar(256) as card_ar_code
            ,lmt_amt.avy_amt::numeric(32,5) as lmt_amt
            ,branch.unq_id_in_src_stm::varchar(256) as opn_br_code
            ,ar.opn_br_id::varchar(256) as opn_br_id
            ,ar.unq_id_in_src_stm::varchar(256) as unq_id_in_src_stm
            ,unlock_dt.ACT_STRT_DT::date as last_actvn_dt
            ,cst_t24.OBJ_IP_ID::char(32) as cst_id
            ,cst_t24.unq_id_in_src_stm::varchar(256) as cst_code
            ,ar.eff_dt::date as eff_fm_dt
            ,ar.end_dt::date as eff_to_dt
            ,source.src_stm_code::varchar(256) as src_stm
        from {{ ref('card_ar') }} ar
            inner join {{ ref('cl_val') }} cv on ar.ar_tp_id = cv.cl_id
            inner join {{ ref('card_ar') }} prn_ar on ar.prn_ar_id = prn_ar.card_ar_id
            inner join {{ ref('src_stm') }} source on ar.src_stm_id = source.src_stm_id
            inner join {{ ref('cl_val') }} status on ar.card_lcs_tp_id = status.CL_ID
            inner join {{ ref('ip') }} branch on ar.OPN_BR_ID = branch.IP_ID
            inner join lmt_amt lmt_amt on ar.card_ar_id = lmt_amt.card_ar_id
            inner join unlock_dt unlock_dt on ar.card_ar_id = unlock_dt.card_ar_id
            inner join cst_t24 cst_t24 on ar.cst_id = cst_t24.SBJ_IP_ID
        where
            cv.cl_code = 'W4-CARD-CTR'
    ),
smy as
    (
        select
            pymtc_id
            ,snpst_dt
            ,card_ar_id
            ,pymtc_code
            ,card_ar_code
            ,lmt_amt
            ,opn_br_code
            ,opn_br_id
            ,unq_id_in_src_stm
            ,last_actvn_dt
            ,cst_id
            ,cst_code
            ,eff_fm_dt
            ,eff_to_dt
            ,src_stm
        from {{ ref('card_smy') }}
        where snpst_dt = REPLACE('{{ var('etl_date') }}', '-', '')::int
    ),
check_minus as
    (
        select 'awm'::varchar as source, awm.* from awm
            minus
        select 'awm'::varchar as source, smy.* from smy

        union all
        
        select 'smy'::varchar as source, smy.* from smy
            minus
        select 'smy'::varchar as source, awm.* from awm
    )
select * from check_minus