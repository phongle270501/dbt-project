{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_fct','test_dmt'])}}

with smy as 
    (
        select
            card.SNPST_DT as cdr_dt
            ,card_dim.card_dim_id as card_dim_id
            ,cst_dim.CST_DIM_ID as cst_dim_id
            ,card_ar_dim.CARD_AR_DIM_ID as card_ar_dim_id
            ,ou_dim.br_dim_id as opn_br_dim_id
            ,nvl(card.LMT_AMT,0) as lmt_amt
            ,DATEDIFF(DAY,card.EFF_FM_DT,'{{var("etl_date") }}'::date) as dys_since_opn
        from {{ ref('card_smy') }} card
            inner join {{ ref('card_dim') }} card_dim on card.pymtc_id = card_dim.CARD_ID
                and card_dim.tf_created_at <= '{{var("etl_date") }}'::date 
                and card_dim.tf_updated_at > '{{var("etl_date") }}'::date 
            inner join {{ ref('card_ar_dim') }} card_ar_dim on card.CARD_AR_ID = card_ar_dim.CARD_AR_ID
                and card_ar_dim.tf_created_at <= '{{var("etl_date") }}'::date 
                and card_ar_dim.tf_updated_at > '{{var("etl_date") }}'::date
            inner join {{ ref('br_dim') }} ou_dim on card.OPN_BR_ID = ou_dim.BR_ID
                and ou_dim.tf_created_at <= '{{var("etl_date") }}'::date 
                and ou_dim.tf_updated_at > '{{var("etl_date") }}'::date  
            inner join {{ ref('cst_dim') }} cst_dim on card.CST_ID = cst_dim.CST_ID
                and cst_dim.tf_created_at <= '{{var("etl_date") }}'::date 
                and cst_dim.tf_updated_at > '{{var("etl_date") }}'::date           
        where card.SNPST_DT = REPLACE('{{ var('etl_date') }}', '-', '')::int
),
fct as
    (
        select 
            cdr_dt
            ,card_dim_id
            ,cst_dim_id
            ,card_ar_dim_id
            ,opn_br_dim_id
            ,lmt_amt
            ,dys_since_opn
        from {{ ref('card_fct') }} card
        where cdr_dt = REPLACE('{{ var('etl_date') }}', '-', '')::int
    ),
check_minus as
    (
        select 'smy'::varchar as source, smy.* from smy
            minus
        select 'smy'::varchar as source, fct.* from fct

        union all
        
        select 'fct'::varchar as source, fct.* from fct
            minus
        select 'fct'::varchar as source, smy.* from smy
    )
select * from check_minus