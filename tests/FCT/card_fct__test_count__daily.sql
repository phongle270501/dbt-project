{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_fct','test_dmt'])}}

with count_stg as 
    (
        select 'card_smy'::varchar as source, count(*) as count_row
        from {{ ref('card_smy') }} card
            inner join {{ ref('card_dim') }} card_dim on card.pymtc_id = card_dim.CARD_ID
                and card_dim.tf_created_at <= '{{var("etl_date") }}'::date 
                and card_dim.tf_updated_at > '{{var("etl_date") }}'::date 
            inner join {{ ref('card_ar_dim') }} card_ar_dim on card.CARD_AR_ID = card_ar_dim.CARD_AR_ID
                and card_ar_dim.tf_created_at <= '{{var("etl_date") }}'::date 
                and card_ar_dim.tf_updated_at > '{{var("etl_date") }}'::date
            inner join {{ ref('br_dim') }} ou_dim on card.OPN_BR_ID = ou_dim.BR_ID
                and ou_dim.tf_created_at <= '{{var("etl_date") }}'::date
                and ou_dim.tf_updated_at > '{{var("etl_date") }}'::date  
            inner join {{ ref('cst_dim') }} cst_dim on card.CST_ID = cst_dim.CST_ID
                and cst_dim.tf_created_at <= '{{var("etl_date") }}'::date 
                and cst_dim.tf_updated_at > '{{var("etl_date") }}'::date           
        where card.SNPST_DT = REPLACE('{{ var('etl_date') }}', '-', '')::int
    ),
count_awm as
    (
        select 'card_fct'::varchar as source, count (*) as count_row
        from {{ ref('card_fct') }} card
        where cdr_dt = REPLACE('{{ var('etl_date') }}', '-', '')::int
    ),
check_count as
    (
        select
            {{dbt_date.today()}} ppn_dt,
            'Test_count'::varchar as Test_type,
            a.source as source_tbl,
            b.source as target_tbl,
            a.count_row as count_source,
            b.count_row as count_target
        from count_stg a 
        full join count_awm b on 1=1
    )

    select 
        *
    from check_count where count_source <> count_target
    union all
    select 
        *
    from check_count where count_target = 0