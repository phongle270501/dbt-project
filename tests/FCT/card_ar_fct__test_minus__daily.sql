{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_fct','test_dmt'])}}

with smy as 
    (
 select  
        card_ar.UNQ_ID_IN_SRC_STM::varchar as CARD_NO,
        card_ar.CARD_AR_ID::varchar as CARD_ID,
        card_ar.SNPST_DT as CDR_DT,
        card_ar.OPN_BR_CODE::varchar as BR_NO,
        card_ar.OPN_BR_ID::varchar as BR_ID,
        card_ar.CST_CODE::varchar as CST_NO,
        card_ar.CST_ID::varchar as CST_ID,     
        coalesce(bsh.CLS_BAL_AMT,0) as CLS_BAL_AMT,
        coalesce(bsh.ACR_INT_AMT,0) as ACR_INT_AMT,
        coalesce(card_ar.NBR_ODUE_DAY,0) as NBR_ODUE_DAY,
        coalesce(bsh.ACR_INT_AMT_LAST_MO,0) as ACR_INT_AMT_LAST_MO,
        coalesce(card_ar.TOT_REPYMT_AMT_MTD,0) as TOT_REPYMT_AMT_MTD
        from {{ ref('card_ar_smy') }} card_ar
        left join
            (select card_ar_id,
                 CLS_BAL_AMT,
                    ACR_INT_AMT,
                    ACR_INT_AMT_LAST_MO
              from  {{ source('deltalake','intr_dbk__card_bal_smy')}} bsh1
                 where bsh1.SNPST_DT = '{{ var("etl_date") }}'::date
                 )     bsh  on card_ar.card_ar_id = bsh.card_ar_id 
        where card_ar.SNPST_DT = REPLACE('{{ var('etl_date') }}', '-', '')::int
),
fct as
    (
        select 
            card_dim.unq_id_in_src_stm::varchar CARD_NO,
            card_dim.card_ar_id::varchar CARD_ID,
            CDR_DT,
            br.unq_id_in_src_stm::varchar BR_NO, 
            br.BR_ID::varchar BR_ID,
            cst_dim.unq_id_in_src_stm::varchar CST_NO,
            cst_dim.cst_id::varchar CST_ID,
            coalesce( CLS_BAL_AMT,0) CLS_BAL_AMT,
            coalesce(ACR_INT_AMT,0) ACR_INT_AMT,
            coalesce(NBR_ODUE_DYS,0) NBR_ODUE_DYS,
            coalesce(ACR_INT_AMT_LAST_MO,0) ACR_INT_AMT_LAST_MO,
            coalesce(TOT_REPYMT_AMT_MTD,0) TOT_REPYMT_AMT_MTD   
        from {{ ref('card_ar_fct') }} fct 
        join {{ ref('card_ar_dim') }} card_dim  on fct.CARD_AR_dim_ID = card_dim.CARD_AR_dim_ID and '{{ var("etl_date") }}'::timestamp between card_dim.TF_CREATED_AT and card_dim.TF_UPDATED_AT - 1
        join {{ ref('br_dim') }} br  on  fct.OPN_BR_dim_ID = br.BR_dim_ID  and '{{ var("etl_date") }}'::timestamp between br.TF_CREATED_AT and br.TF_UPDATED_AT - 1
        join {{ ref('cst_dim') }} cst_dim  on fct.CST_dim_ID = cst_dim.CST_dim_ID and '{{ var("etl_date") }}'::timestamp between cst_dim.TF_CREATED_AT and cst_dim.TF_UPDATED_AT - 1
        where CDR_DT = REPLACE('{{ var('etl_date') }}', '-', '')::int
    ),
check_minus as
    (
        select 'smy'::varchar as source, smy.* from smy
            minus
        select 'smy'::varchar as source, fct.* from fct

        union all
        
        select 'fct'::varchar as source, fct.* from fct
            minus
        select 'fct'::varchar as source, smy.* from smy
    )
select * from check_minus
