{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_ip','test_awm'])}}

with stg as 
    (
        select 
            {{ mcr_generate_surrogate_key([{'column': 'cst.customer_code'} , {'text': 'T24_CUSTOMER'}]) }}::VARCHAR as ip_id,
            cst_dtl.name_1::VARCHAR(100) as ip_nm,
            cst.customer_code::VARCHAR(50) unq_id_in_src_stm--,
            --'{{var("etl_date") }}'::DATE  as tf_created_at,
            --'2400-01-01'::DATE  as tf_updated_at
        from {{ ref('stg_t24__customer') }} cst
         join {{ ref('stg_t24__customer_details') }} cst_dtl on  cst.customer_code = cst_dtl.customer_code
        where cst_dtl.M = '1' 
                and cst_dtl.S = '1'
    ),
awm as
    (
        select 
            ip_id,
            ip_nm,
            unq_id_in_src_stm--,
            --tf_created_at,
            --tf_updated_at
        from {{ ref('ip') }}
        where  SRC_STM_ID = {{ mcr_generate_surrogate_key([{'text': 'T24_CUSTOMER'} , {'text': 'CSV_SRC_STM'}]) }}
        and (tf_deleted_at >('{{var("etl_date") }}'::DATE +1) or tf_deleted_at  is null)
    ),
check_minus as
    (
        select 'stg'::varchar as source, stg.* from stg
            minus
        select 'stg'::varchar as source, awm.* from awm

        union all
        
        select 'awm'::varchar as source, awm.* from awm
            minus
        select 'awm'::varchar as source, stg.* from stg
    )
select * from check_minus