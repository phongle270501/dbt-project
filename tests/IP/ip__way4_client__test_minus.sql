{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_ip','test_awm'])}}

with stg as 
    (
        select 
            {{ mcr_generate_surrogate_key([{'column': 'cst.id'} , {'text': 'WAY4_CLIENT'}]) }}::VARCHAR as ip_id,
            cst.short_name::VARCHAR(100) as ip_nm,
            cst.id::VARCHAR(50) unq_id_in_src_stm--,
          --  '{{var("etl_date") }}'::DATE  as tf_created_at,
          --  '2400-01-01'::DATE  as tf_updated_at
        from {{ ref('stg_way4__client') }} cst

    ),
awm as
    (
        select 
            ip_id,
            ip_nm,
            unq_id_in_src_stm--,
            --tf_created_at,
            --tf_updated_at
        from {{ ref('ip') }}
        where  SRC_STM_ID = {{ mcr_generate_surrogate_key([{'text': 'WAY4_CLIENT'} , {'text': 'CSV_SRC_STM'}]) }}
        and (tf_deleted_at >('{{var("etl_date") }}'::DATE +1) or tf_deleted_at  is null)
    ),
check_minus as
    (
        select 'stg'::varchar as source, stg.* from stg
            minus
        select 'stg'::varchar as source, awm.* from awm

        union all
        
        select 'awm'::varchar as source, awm.* from awm
            minus
        select 'awm'::varchar as source, stg.* from stg
    )
select * from check_minus