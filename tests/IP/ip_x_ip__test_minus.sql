{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_ip','test_awm'])}}

with w4_cst as (
    select 
        w4_cst.id,
        CASE 
            WHEN w4_cst.REG_NUMBER LIKE '999%' AND w4_cst.CCAT = 'C' THEN SUBSTRING(w4_cst.REG_NUMBER, 4)
            ELSE w4_cst.REG_NUMBER
        END AS REG_NUMBER,
        w4_cst.CLIENT_NUMBER
    from {{ ref('stg_way4__client') }} w4_cst
    where 
        w4_cst.AMND_STATE = 'A'
),
priority1 as (
				select 	t24_cst.CUSTOMER_CODE, 
						t24_cst_dtl.LEGAL_ID,
						1::int Uutien1
						--,t24_cst_dtl.name_1
					 from {{ ref('stg_t24__customer') }}	t24_cst
				 join {{ ref('stg_t24__customer_details') }} t24_cst_dtl on	t24_cst.CUSTOMER_CODE = t24_cst_dtl.CUSTOMER_CODE
									and t24_cst_dtl.M = '1'
									and t24_cst_dtl.S = '1'
				 where t24_cst_dtl.name_1 not like '%KHONG%SU%DUNG%'
				 and t24_cst_dtl.name_1 not like  '%Ko%SU%DUNG%'
				 and t24_cst_dtl.name_1 not like  '%K SU DUNG%'
				 and t24_cst_dtl.name_1 not like  '%Khong Sd%'
		 	),
priority2 as (
		 
		 		select 	t24_cst.CUSTOMER_CODE, 
						t24_cst_dtl.DOC_NUM,
						2::int Uutien1
				from {{ ref('stg_t24__customer') }}	t24_cst
				 join {{ ref('stg_t24__customer_details') }}  t24_cst_dtl on	t24_cst.CUSTOMER_CODE = t24_cst_dtl.CUSTOMER_CODE
									and t24_cst_dtl.M = '1'
									and t24_cst_dtl.S = '1'
				 where t24_cst_dtl.name_1 not like '%KHONG%SU%DUNG%'
				 and t24_cst_dtl.name_1 not like  '%Ko%SU%DUNG%'
				 and t24_cst_dtl.name_1 not like  '%K SU DUNG%'
				 and t24_cst_dtl.name_1 not like  '%Khong Sd%'
				 
		 ),
		 
priority3 as (		 
		 		select 	t24_cst.CUSTOMER_CODE, 
						t24_cst_dtl.CUSTOMER_CODE CUSTOMER_CODE1,
						3::int Uutien1
				from {{ ref('stg_t24__customer') }}	t24_cst 
				 join {{ ref('stg_t24__customer_details') }} t24_cst_dtl on	t24_cst.CUSTOMER_CODE = t24_cst_dtl.CUSTOMER_CODE
									and t24_cst_dtl.M = '1'
									and t24_cst_dtl.S = '1'
				 where t24_cst_dtl.name_1 not like '%KHONG%SU%DUNG%'
				 and t24_cst_dtl.name_1 not like  '%Ko%SU%DUNG%'
				 and t24_cst_dtl.name_1 not like  '%K SU DUNG%'
				 and t24_cst_dtl.name_1 not like  '%Khong Sd%'
		),
merch_all as 
		(
		select w4_cst.id ID, 
				p1.CUSTOMER_CODE CST_CODE, 
				p1.Uutien1, 
				case when w4_cst.CLIENT_NUMBER = p1.CUSTOMER_CODE then 0::int else p1.CUSTOMER_CODE::int end uutien2 
		from  w4_cst
		join priority1 p1 on w4_cst.REG_NUMBER = p1.LEGAL_ID
		union 
		select w4_cst.id ID, p2.CUSTOMER_CODE CST_CODE, p2.Uutien1, case when w4_cst.CLIENT_NUMBER = p2.CUSTOMER_CODE then 0::int else p2.CUSTOMER_CODE::int end uutien2 
		from  w4_cst
		join priority2 p2 on w4_cst.REG_NUMBER = p2.doc_num
		union 
		select w4_cst.id ID, p3.CUSTOMER_CODE CST_CODE, p3.Uutien1, 0::int uutien2 
		from  w4_cst
		join priority3 p3 on w4_cst.client_number = p3.CUSTOMER_CODE
		),
ketqua as (	
select  ID,
		CST_CODE,
        Uutien1,
        uutien2,
		row_number () over(partition by ID order by Uutien1 asc, uutien2 asc) row_num
		from merch_all
),
stg as(
select  ID::varchar CST_W4,
        'WAY4_CLIENT' CST_W4_TP,
        'CST-TO-T24CIF' IP_X_IP_TP,
        CST_CODE::varchar CST_T24,
        'T24_CUSTOMER' CST_T24_TP
 from ketqua
 where row_num = 1
)
,
awm as
    (
		select ip.unq_id_in_src_stm CST_W4, 
        s1.SRC_STM_CODE CST_W4_TP,
        tp.cl_code IP_X_IP_TP,
         ip2.unq_id_in_src_stm CST_T24,
         s2.SRC_STM_CODE CST_T24_TP
		from {{ ref('ip_x_ip') }}	 ip_rlt
		join {{ ref('ip') }}	 ip on ip_rlt.sbj_ip_id = ip.ip_id
		join {{ ref('ip') }}	 ip2 on ip_rlt.obj_ip_id = ip2.ip_id
        join {{ ref('src_stm') }}	 s1 on ip.src_stm_id = s1.src_stm_id
        join {{ ref('src_stm') }}	 s2 on ip2.src_stm_id = s2.src_stm_id
        join {{ ref('cl_val') }}	 tp on ip_rlt.ip_x_ip_tp_id = tp.cl_id
		where 1=1
		--and '{{var("etl_date") }}'::DATE between ip_rlt.tf_created_at and ip_rlt.tf_updated_at -1
		and '{{var("etl_date") }}'::DATE between ip_rlt.tf_created_at and ip_rlt.tf_updated_at -1
    ),
check_minus as
    (
        select 'stg'::varchar as source, stg.* from stg
            minus
        select 'stg'::varchar as source, awm.* from awm

        union all
        
        select 'awm'::varchar as source, awm.* from awm
            minus
        select 'awm'::varchar as source, stg.* from stg
    )
select * from check_minus