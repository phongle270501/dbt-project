{% macro build_snapshot_table(strategy, sql) %}

    select *,
        {{ strategy.scd_id }} as dbt_scd_id,
        {{ strategy.updated_at }} as dbt_updated_at,
        -- {{ strategy.updated_at }} as dbt_valid_from,
        '{{ var("etl_date") }}'::timestamp as dbt_valid_from,
        --nullif({{ strategy.updated_at }}, {{ strategy.updated_at }}) as dbt_valid_to
        '{{ var("end_date") }}'::timestamp as dbt_valid_to
    from (
        {{ sql }}
    ) sbq

{% endmacro %}