{% macro mcr_get_bussiness_columns(target_relation, identity_col=none) %}    
    {%- set tech_cols = ['TF_CREATED_AT', 'TF_UPDATED_AT','PPN_DT','PPN_TM','JOB_NM', 'TF_DELETED_AT'] -%}

    {%- if identity_col is not none %}
        {{- tech_cols.append(identity_col|upper) or "" -}}
    {%- endif -%}

    {%- set final_tech_cols = tech_cols %}
    
    {%- if is_incremental() %}
        {%- set columns = adapter.get_columns_in_relation(target_relation) -%}
        {%- set column_names = [] -%}
        {%- for column in columns -%}            
            {%- if column.name|upper not in final_tech_cols -%}
                {{- column_names.append(column.name) or "" -}}                
            {#{%- else -%}
                {{ log("exclude column: " ~ column.name, info=true) }}#}
            {%- endif -%} 
        {%- endfor -%}    
        {# {{- "Column: " ~ column_names -}} #}
    {%- else -%}
        {%- set column_names = ['TF_CREATED_AT'] -%}
        {# {{- "Column in else: " ~ column_names -}} #}
    {%- endif -%}
    {{return(column_names)}}  
{% endmacro %}


{% macro mcr_get_bussiness_columns_v2(target_relation, alias_nm, identity_col=none) %}    
    {%- set tech_cols = ['TF_CREATED_AT', 'TF_UPDATED_AT','PPN_DT','PPN_TM','JOB_NM', 'TF_DELETED_AT'] -%}

    {%- if identity_col is not none %}
        {{- tech_cols.append(identity_col|upper) or "" -}}
    {%- endif -%}

    {%- set final_tech_cols = tech_cols %}
    
    {%- if is_incremental() %}
        {%- set columns = adapter.get_columns_in_relation(target_relation) -%}
        {%- set column_names = [] -%}
        {%- for column in columns -%}            
            {%- if column.name|upper not in final_tech_cols -%}
                {{- column_names.append(alias_nm ~'.'~column.name) or "" -}}                
            {#{%- else -%}
                {{ log("exclude column: " ~ column.name, info=true) }}#}
            {%- endif -%} 
        {%- endfor -%}    
        {#{{- log("Column: " ~ column_names, info=true) -}} #}
    {%- else -%}
        {%- set column_names = ['TF_CREATED_AT'] -%}
    {%- endif -%}
    {{return(column_names)}}  
{% endmacro %}

{# nam.do add: get type 2 column in a scd type 3 table #}
{% macro mcr_get_bussiness_columns_v3(target_relation, alias_nm, identity_col=none, type1_col=[], unique_key=[]) %}    
    {%- set tech_cols = ['TF_CREATED_AT', 'TF_UPDATED_AT','PPN_DT','PPN_TM','JOB_NM', 'TF_DELETED_AT'] -%}

{# exclude identity column #}
    {%- if identity_col is not none %}
        {{- tech_cols.append(identity_col|upper) or "" -}}
    {%- endif -%}
{# end - exclude entity column #}
    {# {{ log("log lan 1 identity_col: " ~ identity_col ~ ' and tech_col: ' ~ tech_cols, info=true) }} #}

{# exclude type 1 columns #}
    {%- if type1_col is not none %}
        {%-  set new_type1_col = []  -%}
        {%- for col in type1_col %}
            {{ new_type1_col.append(col|upper) or "" }}
        {% endfor %}
        {{- tech_cols.extend(new_type1_col) or "" -}}
    {%- endif -%}
{# end - exclude type 1 columns #}

    {# {{ log("log lan 2 identity_col: " ~ identity_col ~ ' and tech_col: ' ~ tech_cols, info=true) }} #}

{# exclude unique_key columns #}
    {%- if unique_key is not none %}
        {%-  set new_unique_key = []  -%}
        {%- for col in unique_key %}
            {{ new_unique_key.append(col|upper) or "" }}
        {% endfor %}
        {{- tech_cols.extend(new_unique_key) or "" -}}
    {%- endif -%}
{# end - exclude unique_key columns #}

    {%- set final_tech_cols = tech_cols %}

    {%- if is_incremental() %}
        {%- set columns = adapter.get_columns_in_relation(target_relation) -%}
        {%- set column_names = [] -%}
        {%- for column in columns -%}            
            {%- if column.name|upper not in final_tech_cols -%}
                {{- column_names.append(alias_nm ~'.'~column.name) or "" -}}                
            {#{%- else -%}
                {{ log("exclude column: " ~ column.name, info=true) }}#}
            {%- endif -%} 
        {%- endfor -%}    
        {#{{- log("Column: " ~ column_names, info=true) -}} #}
    {%- else -%}
        {%- set column_names = ['TF_CREATED_AT'] -%}
    {%- endif -%}
    {{return(column_names)}}  
{% endmacro %}