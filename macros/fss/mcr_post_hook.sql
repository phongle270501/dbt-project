{% macro process_in_target_table(source_table, current_table, write_mode, unique_keys) %} 
    {# process_in_target_table( this, hook_tbl,  'scd1', ['ip_id']) #}

    {%- set columns = adapter.get_columns_in_relation(source_table) -%}
    {% set csv_colums = get_quoted_csv(columns | map(attribute="name")) %}
    {% set ns = namespace(strJoin = '') %}
        {% for col in unique_keys %}
            {% set ns.strJoin = ns.strJoin ~ current_table ~'.'~  col  ~ '=' ~ 'src.'  ~col  %}
            {%- if not loop.last %} 
                {% set ns.strJoin = ns.strJoin ~' and ' %}
            {% endif -%}            
        {% endfor %} 

    {% set str_query  %} 
        begin transaction;
            DELETE FROM {{current_table}}
            USING {{source_table}} src
            WHERE {{ns.strJoin}};

            INSERT INTO  {{current_table}} ( {{csv_colums}} )
            SELECT {{csv_colums}} 
            FROM  {{source_table}};

            COMMIT;
        end transaction;
    {% endset %}
    
    {% set results = run_query(str_query) %}
    {% if execute %}
        {{log(''~str_query, info=true)}}
        {% set result_rows = results.rows %}
    {% else %}
        {% set result_rows = [] %}
    {% endif %}
    select 1=1
{% endmacro %}
{# 
begin transaction;
            DELETE FROM {{current_table}}
            USING {{source_table}} src
            WHERE {{ns.strJoin}};

            INSERT INTO  {{current_table}} ( {{csv_colums}} )
            SELECT {{csv_colums}} 
            FROM  {{source_table}};

            COMMIT;
        end transaction; #}