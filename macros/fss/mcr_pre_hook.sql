{% macro scd2_revert(schema_name , target_relation) %}  
    
    DELETE FROM  {{schema_name}}.{{target_relation.table }}
    WHERE tf_created_at >= '{{ var("etl_date") }}'::timestamp;

    UPDATE {{schema_name}}.{{target_relation.table }}
    SET tf_updated_at = '{{ var("end_date") }}'::timestamp
    WHERE tf_updated_at =  '{{ var("etl_date") }}'::timestamp
{% endmacro %}


{% macro incremental_delete_day(column, day) %}
  {% if is_incremental() %}
    DELETE FROM {{this}} WHERE {{column}}::DATE >= DATEADD(DAY, -{{day}}, CURRENT_DATE)::DATE
  {% endif %}
{% endmacro %}