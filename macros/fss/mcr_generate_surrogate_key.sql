
{%- macro mcr_generate_surrogate_key(arg_list) -%}
    {%- set default_null_value = "_dbt_utils_surrogate_key_null_" -%}
    {%- set fields = [] -%}
    {%- set conditions = [] -%}

    {%- for arg in arg_list -%}
        {%- if arg.get('column') is not none -%}
            {%- set column = arg.get('column') -%} 
            {%- do conditions.append(
                column ~ " is null " 
            ) -%}
        {%- endif -%}  
    {%- endfor -%}
        
    {%- for arg in arg_list -%}
        {%- if arg.get('column') is none -%}
            {%- set text = arg.get('text') -%}
            {%- do fields.append(
                "'" ~ text ~ "'"
            ) -%}
        {%- else -%}
            {%- set column = arg.get('column') -%}            
            {%- do fields.append(
                "coalesce(cast(" ~ column ~ " as TEXT), '" ~ default_null_value  ~ "')"
            ) -%}
            

        {%- endif -%}
    {%- endfor -%}

    {% set result_str =  "md5(cast(" ~ fields|join(" || '-' || ") ~ " as TEXT))" %}

    {%- if conditions is defined and conditions|length > 0  %} 
        {%- set condition_str = 'case when ( ' ~ conditions|join(' OR ') ~ ') then null else ' ~ result_str  ~ ' end ' -%}
    {%- else -%}
        {%- set condition_str = result_str -%}
    {%- endif -%}

    {{return(condition_str)}} 
{%- endmacro %}


