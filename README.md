Welcome to your new dbt project!

### PREPARE FOR PROJECT DBT ###
### 1. clone project => DEV environment
git clone -b dev --single-branch <link_project_gitlab> 

### 2. prepare env-dbt & install package for project (in the first time)
dbt-core
dbt-redshift
dbt deps

### 3. assume profiles.yml file unchanged in local repository
git update-index --assume-unchanged profiles.yml

### 4. add connection to DB in profiles.yml

### 5. push change code to remote dev branch (SIT)
git add <file-change>
git commit -m "<user> command..."
git pull origin dev # should pull before push to update newest code and avoid conflict
git push origin dev

### Using the starter project ###
Try running the following commands:
- dbt deps
- dbt debug
- dbt run
- dbt test

### Resources:
- Learn more about dbt [in the docs](https://docs.getdbt.com/docs/introduction)
- Check out [Discourse](https://discourse.getdbt.com/) for commonly asked questions and answers
- Join the [chat](https://community.getdbt.com/) on Slack for live discussions and support
- Find [dbt events](https://events.getdbt.com) near you
- Check out [the blog](https://blog.getdbt.com/) for the latest news on dbt's development and best practices

testaaaeeeeaaa