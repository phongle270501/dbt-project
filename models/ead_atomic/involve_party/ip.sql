{#
    job name : ip
    mapping ID : EAD_RA IP
    created by : phong.le
    modified by | date modify | description
    phong.le    | 2023-01-11  | update code
#}

-- get bussiness column:
{% set column_names = mcr_get_bussiness_columns(this) %}

--- config job:
{{
    config(
        materialized = 'scd',
        unique_key = ['ip_id'],
        strategy = 'type1',
        partial_parse = 'True',
        tags = ['awm', 'ip']
    )
}}

--- get data from t24_branch, t24_customer and w4_client

with t24_branch as (
    select * from {{ ref('intr_awm_ip__t24_branch') }} 
),

t24_customer as (
    select * from {{ ref('intr_awm_ip__t24_customer') }} 
),

way4_customer as (
    select * from {{ ref('intr_awm_ip__way4_customer') }} 
),


union_all as (
    select * from t24_branch 
    union all 
    select * from t24_customer 
    union all 
    select * from way4_customer

),

final as (

    select 
        a.*
        , {{ dbt_utils.generate_surrogate_key( column_names )}} compare_key
        , '{{ var("end_date") }}'::date as tf_updated_at
    from union_all a
)

select 
    {{'s.'~column_names| join(',s.')}}
    --------------------------
    ,'{{ model.path }}' job_nm
    , s.tf_created_at 
    , s.tf_updated_at    
    ,{{dbt_date.today()}} ppn_dt
    ,{{dbt_date.now()}}::time ppn_tm 
    , null::date tf_deleted_at
    , s.compare_key
from final s
