{#
    job name : card_ar_avy
    mapping ID : EAD_RA CARD_AR_AVY
    created by : tien.cu
    modified by | date modify | description
    tien.cu     | 2023-09-04  | update code
#}

{% set column_names = mcr_get_bussiness_columns(this) %}
--- config job
{{ 
    config(
        materialized='incremental',
        unique_key=['CARD_AR_AVY_ID','tf_created_at'],
        incremental_strategy='delete+insert',
        tags = ['awm', 'card_ar_avy']
    ) 
}}

--- declare all model in use
with unlock_date as (

    select 
        CARD_AR_ID
        ,CARD_AR_AVY_ID
        ,ACT_STRT_DT
        ,cast( null as NUMERIC(32,5)) AVY_AMT
        ,AVY_TP_ID
        ,TF_CREATED_AT
    from {{ ref('intr_awm_card_ar_avy__unlock_date') }}  where TF_CREATED_AT = '{{ var("etl_date") }}'

)

,limit_amt as (

    select 
        CARD_AR_ID
        ,CARD_AR_AVY_ID
        ,null::date as ACT_STRT_DT
        ,AVY_AMT
        ,AVY_TP_ID
        ,TF_CREATED_AT
    from {{ ref('intr_awm_card_ar_avy__limit_amt') }}  where TF_CREATED_AT = '{{ var("etl_date") }}'

)

,overdue_date as (
    
    select 
        CARD_AR_ID
        ,CARD_AR_AVY_ID
        ,ACT_STRT_DT
        ,cast( null as NUMERIC(32,5)) AVY_AMT
        ,AVY_TP_ID
        ,TF_CREATED_AT
    from {{ ref('intr_awm_card_ar_avy__overdue_date') }} where TF_CREATED_AT = '{{ var("etl_date") }}'

)

,intr as (
    select * from unlock_date
    union all
    select * from limit_amt
    union all
    select * from overdue_date
)
, intr_row as (
  
    select  a.*
           ,{{ dbt_utils.generate_surrogate_key( column_names )}} compare_key
           ,'{{ var("end_date") }}'::date tf_updated_at
           ,{{dbt_date.today()}} ppn_dt
           ,{{dbt_date.now()}}::time ppn_tm
           ,'{{ model.path }}' job_nm
    from intr a
)

--- process if is daily run
{% if is_incremental() %}
    , destination_rows as (---lay cac ban ghi co hieu luc o thoi diem hien tai    
        select *
               , {{ dbt_utils.generate_surrogate_key( column_names )}} compare_key
         from {{ this }} 
        where tf_updated_at = cast('{{ var("end_date") }}' as date)
    
    )

    , new_valid_to as (---xac dinh thong tin ban ghi moi duoc insert vao    
        select s.CARD_AR_AVY_ID
            , s.CARD_AR_ID
            , s.ACT_STRT_DT
            , s.AVY_AMT
            , s.AVY_TP_ID
            , s.tf_created_at 
            , s.tf_updated_at  ---end date
            , s.ppn_dt
            , s.ppn_tm  -- thong tin la thong tin moi nhat tu Source, hieu luc tu ngay Etl -> oo    
            , s.job_nm                     
          from intr_row s
          left join destination_rows d
            on s.CARD_AR_AVY_ID = d.CARD_AR_AVY_ID
         where nvl(s.compare_key, '$')  != nvl(d.compare_key, '$')      
    )

    , add_new_valid_to as (---End date ban ghi cu
        select  d.CARD_AR_AVY_ID
                , d.CARD_AR_ID
                , d.ACT_STRT_DT
                , d.AVY_AMT
                , d.AVY_TP_ID
                , d.tf_created_at 
                , '{{ var("etl_date") }}'::date as tf_updated_at  ---end date
                , d.ppn_dt
                , d.ppn_tm
                , n.job_nm
          from destination_rows d
          join new_valid_to n
            on d.CARD_AR_AVY_ID = n.CARD_AR_AVY_ID

    )
    , final as (
        select n.*
        from add_new_valid_to n
        union all
        select o.* from new_valid_to o
    )
{% endif %}

--- final data
select  {{'d.'~column_names| join(',d.')}}
        -----------------
        , d.tf_created_at 
        , d.tf_updated_at  ---end date
        , d.ppn_dt
        , d.ppn_tm
        , d.job_nm
  from intr_row d
  --- process if is daily run
{% if is_incremental() %}
    Where 1=2
    union all 
    Select
    {{'s.'~column_names| join(',s.')}}
    ----------------------
    , s.tf_created_at 
    , s.tf_updated_at  ---end date
    , s.ppn_dt
    , s.ppn_tm
    , s.job_nm 
    from final s
    
{% endif %}