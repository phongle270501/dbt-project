
##### the below are descriptions tenical column in awm tables
{% docs PPN_DT %}Ngày tổng hợp bản ghi gần nhất (do job xử lý dữ liệu cập nhật hoặc bằng tay){% enddocs %}

{% docs PPN_TM %}Thời gian (thời điểm) tổng hợp bản ghi gần nhất (do job xử lý dữ liệu cập nhật hoặc bằng tay){% enddocs %}

{% docs TF_CREATED_AT %} Effective date of record on Data system (Data Warehouse Effective Date, Valid From Date) {% enddocs %}

{% docs TF_UPDATED_AT %} End date of record on Data system. If not Expire then '2400-01-01' (Data Warehouse End Date, Valid To Date) {% enddocs %}

{% docs JOB_NM %} The job name contains the code that generates the record {% enddocs %}