{#
    job name : cl_val
    mapping ID : EAD_RA CL_VAL
    created by : tai
    modified by | date modify | description
    tien.cu    | 2023-09-04  | update code
#}

{% set column_names = mcr_get_bussiness_columns(this) %}
--- config job
{{
    config(
        materialized = 'incremental',
        unique_key = 'CL_ID',
        tags = ["awm_cl_val", "cl", "cl_val"]
    )
}}

--- declare all model in use
with source as(
    select * from {{ ref('stg_csv__csv_cv') }}
) 
, transform as (
    select {{ mcr_generate_surrogate_key([{'column': 'a.scm_code'}, {'column': 'a.cl_code'}, {'text': 'CSV_CV'}]) }} cl_id
         , {{ mcr_generate_surrogate_key([{'text': 'CSV_CV'}, {'text': 'CSV_SRC_STM'}]) }} src_stm_id
         , {{ mcr_generate_surrogate_key([{'column': 'a.scm_code'}, {'text': 'CSV_CL_SCM'}]) }} cl_scm_id
         , scm_code::varchar(64) scm_code
         , shrt_nm::varchar(50) shrt_nm
         , cl_code::varchar(50) cl_code
         , cl_nm::varchar(100)   cl_nm       
         ---technical col
        , '{{ model.path }}' job_nm
        , '{{ var("etl_date") }}'::date as tf_created_at
        , '{{ var("end_date") }}'::date as tf_updated_at
        ---compare column
        , {{ dbt_utils.generate_surrogate_key( column_names )}} compare_key
    from source a     
)
--- process if is daily run
{% if is_incremental() %}
 ,trgt as(
    select  t.*
        ,{{ dbt_utils.generate_surrogate_key( column_names )}} compare_key 
    from {{this}} t
)
{% endif %}

--- final data
select 
    {{'s.'~column_names| join(',s.')}}
    --------------------------------
    {% if is_incremental() %}
        , COALESCE(t.tf_created_at, s.tf_created_at) tf_created_at
        , case when t.CL_ID is null then s.tf_updated_at else '{{ var("etl_date") }}'::date end  tf_updated_at   
    {% else %}
        , s.tf_created_at
        , s.tf_updated_at
    {% endif %}    
    , s.job_nm
    ,{{dbt_date.today()}} ppn_dt
    ,{{dbt_date.now()}}::time ppn_tm
    
from transform s
--- process if is daily run
{% if is_incremental() %}
   left join trgt t ON s.CL_ID = t.CL_ID
   where nvl(t.compare_key,'$') <> nvl(s.compare_key,'$')
{% endif %}