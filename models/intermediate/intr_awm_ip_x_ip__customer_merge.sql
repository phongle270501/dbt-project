    {{
    config(
        materialized='incremental',
        pre_hook = 'truncate table {{this}} ',
        tags = ["intr_awm_ip_x_ip", "ip_x_ip"]
    )
}}


with

t24_cst as (

    select * from {{ ref('stg_t24__customer') }} t24_cst

),

t24_cst_dtl as (

    select t24_cst_dtl.* from {{ ref('stg_t24__customer_details') }} t24_cst_dtl 
    where t24_cst_dtl.name_1 not like '%KHONG%SU%DUNG%' 
    			and t24_cst_dtl.name_1 not like '%Ko%SU%DUNG%'
    			and t24_cst_dtl.name_1 not like '%K SU DUNG%'
    			and t24_cst_dtl.name_1 not like '%Khong Sd%'
        and t24_cst_dtl.M = '1' and t24_cst_dtl.S = '1'

),

w4_cst as (
    select 
        w4_cst.id,
        CASE 
            WHEN w4_cst.REG_NUMBER LIKE '999%' AND w4_cst.CCAT = 'C' THEN SUBSTRING(w4_cst.REG_NUMBER, 4)
            ELSE w4_cst.REG_NUMBER
        END AS REG_NUMBER,
        w4_cst.CLIENT_NUMBER
    from {{ ref('stg_way4__client') }} w4_cst
    where 
        w4_cst.AMND_STATE = 'A'
),

priority1 as (
    select
    w4_cst.ID,
    t24_cst_dtl.CUSTOMER_CODE,
    1 as priority_1,
    case when t24_cst_dtl.customer_code = w4_cst.client_number then 0::int
        else t24_cst_dtl.CUSTOMER_CODE :: int 
        end as priority_2
    from w4_cst 
    join t24_cst_dtl on w4_cst.REG_NUMBER = t24_cst_dtl.LEGAL_ID
    

),

priority2 as (
    select
    w4_cst.ID,
    t24_cst_dtl.CUSTOMER_CODE,
    2 as priority_1,
    case when t24_cst_dtl.customer_code = w4_cst.client_number then 0::int
        else t24_cst_dtl.CUSTOMER_CODE::int
        end as priority_2
    from w4_cst 
    join t24_cst_dtl on w4_cst.REG_NUMBER = t24_cst_dtl.DOC_NUM
    

),

priority3 as (
    select
    w4_cst.ID,
    t24_cst.CUSTOMER_CODE,
    3 as priority_1,
    0::int as priority_2
    from w4_cst 
    join t24_cst on w4_cst.CLIENT_NUMBER = t24_cst.customer_code

),

union_all as(
    select * from priority1
    union all 
    select * from priority2
    union all 
    select * from priority3
    
),



final as (
    select 
        {{ mcr_generate_surrogate_key([{'column': 'a.ID'}, {'text': 'WAY4_CLIENT'}]) }} as SBJ_IP_ID,
        {{ mcr_generate_surrogate_key([{'text': 'IP-X-IP-TP'}, {'text': 'CST-TO-T24CIF'}, {'text':'CSV_CV'}]) }} as IP_X_IP_TP_ID,
        {{ mcr_generate_surrogate_key([{'column': 'a.CUSTOMER_CODE'} , {'text': 'T24_CUSTOMER'}]) }} as OBJ_IP_ID,
        {{ mcr_generate_surrogate_key([{'text': 'WAY4_CLIENT'} , {'text': 'CSV_SRC_STM'}]) }} as SRC_STM_ID,
        '{{ var("etl_date") }}'::date as TF_CREATED_AT,
        row_number() over (partition by a.ID order by a.priority_1 asc , a.priority_2 asc) row_num
    from union_all a

)

select SBJ_IP_ID,IP_X_IP_TP_ID,OBJ_IP_ID,SRC_STM_ID,TF_CREATED_AT from final
where row_num = 1



