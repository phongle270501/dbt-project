{{
    config(
        materialized='incremental',
        pre_hook = 'truncate table {{this}} ',
        tags = ['awm','card_ar_avy', 'intr_awm_card_ar_avy__limit_amt']
    )
}}


with

acnt_ctr as (

    select * from {{ ref('stg_way4__tbl_w4_acnt_contract') }}

),

final as (
    select 
        {{ mcr_generate_surrogate_key([{'column': 'a.ID'}, {'text': 'WAY4_ACNT_CONTRACT'}]) }} as CARD_AR_ID,
        {{ mcr_generate_surrogate_key([{'column': 'a.ID'}, {'text': 'W4-CARD-CTR-LMT-AMT'}, {'text':'WAY4_ACNT_CONTRACT'}]) }} as CARD_AR_AVY_ID,
        a.ACCOUNT_LIMIT AVY_AMT,
        {{ mcr_generate_surrogate_key([{'text': 'AR-AVY-TP'}, {'text': 'W4-CARD-CTR-LMT-AMT'} , {'text': 'CSV_CV'}]) }} as AVY_TP_ID,
        '{{ var("etl_date") }}'::date as TF_CREATED_AT
    from acnt_ctr a
    where a.CON_CAT = 'C'
    and a.ACCOUNT_LIMIT is not null
)

select * from final