{{
    config (
        materialized='incremental',
        pre_hook = 'truncate table {{this}}',
        tags = ['intr','dtm', 'fact', 'card_ar_fct']
    )
}}

with card_ar_smy as (
    select * from {{ ref('card_ar_smy') }}
) 
, card_bal_smy as (
    select * from {{ source('deltalake', 'intr_dbk__card_bal_smy') }}
) 
, card_ar as (
    select *
    from card_ar_smy card_ar
    where card_ar.SNPST_DT = REPLACE('{{ var('etl_date') }}', '-', '')::INT
)
, bsh as (
    select bsh.CARD_AR_ID
        ,CLS_BAL_AMT
        ,ACR_INT_AMT
    from card_bal_smy bsh
)
, bsh_mo as (
    select bsh_mo.CARD_AR_ID
        , ACR_INT_AMT_LAST_MO
    from card_bal_smy bsh_mo
)
, card_dim as (
    select *
    from {{ ref('card_ar_dim') }} card_dim
    where '{{ var("etl_date") }}'::date >= card_dim.TF_CREATED_AT and '{{ var("etl_date") }}'::date < card_dim.TF_UPDATED_AT
)
, ou_dim as (
    select *
    from {{ ref('br_dim') }} ou_dim
    where '{{ var("etl_date") }}'::date >= ou_dim.TF_CREATED_AT and '{{ var("etl_date") }}'::date < ou_dim.TF_UPDATED_AT
)
, cst_dim as (
    select *
    from {{ ref('cst_dim') }} cst_dim
    where '{{ var("etl_date") }}'::date >= cst_dim.TF_CREATED_AT and '{{ var("etl_date") }}'::date < cst_dim.TF_UPDATED_AT
)
, final as (
    select 
        card_dim.CARD_AR_DIM_ID
        ,card_ar.SNPST_DT as CDR_DT
        ,ou_dim.BR_DIM_ID as OPN_BR_DIM_ID
        ,cst_dim.CST_DIM_ID
        ,nvl(bsh.CLS_BAL_AMT,0) as CLS_BAL_AMT
        ,nvl(bsh.ACR_INT_AMT,0) as ACR_INT_AMT
        ,card_ar.NBR_ODUE_DAY as NBR_ODUE_DYS
        ,nvl(bsh_mo.ACR_INT_AMT_LAST_MO,0) as ACR_INT_AMT_LAST_MO
        ,nvl(card_ar.TOT_REPYMT_AMT_MTD,0) as TOT_REPYMT_AMT_MTD
    from card_ar
    left join bsh on card_ar.CARD_AR_ID = bsh.CARD_AR_ID
    left join bsh_mo on card_ar.CARD_AR_ID = bsh_mo.CARD_AR_ID
    inner join card_dim on card_ar.CARD_AR_ID = card_dim.CARD_AR_ID
    inner join ou_dim on card_ar.OPN_BR_ID = ou_dim.BR_ID
    inner join cst_dim on card_ar.CST_ID = cst_dim.CST_ID
)
select * from final