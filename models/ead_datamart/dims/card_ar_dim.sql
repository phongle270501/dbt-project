{#
    job name : card_ar_dim
    mapping ID : DM CARD_AR_DIM
    created by : nam.tran
    modified by | date modify | description
    tien.cm     | 2023-09-04  | update cdode
#}

{% set path = model.path %}
{% set column_names = mcr_get_bussiness_columns(this, 'card_ar_dim_id') %}
{% set snpst_dt = var("etl_date")|replace('-','') %}

--- config job
{{
    config(
        materialized = 'scd',
        full_refresh = false,
        unique_key= ['card_ar_id'],
        identity_col = 'card_ar_dim_id',        
        strategy = 'type2',
        pre_hook = scd2_revert('datamart', this),        
        tags=['dtm','dim', 'card_ar_dim']
    )
}}

--- declare all model in use
with source as (

    select ar.card_ar_code card_ar_nbr
        ,ar.card_ar_id
        ,ar.src_stm
        ,ar.unq_id_in_src_stm::integer
        ,ar.eff_to_dt eff_mat_dt
        ,ar.lc_st
    from {{ ref('card_ar_smy')}}  ar
    where  snpst_dt = {{snpst_dt}}
    
)
, source_rows as (
  
    select  {{'ar.'~column_names| join(',ar.')}}
           -------------technical columns-----------      
           , '{{ var("etl_date") }}'::date as tf_created_at
           , '{{ var("end_date") }}'::timestamp as tf_updated_at
           , {{dbt_date.today()}} ppn_dt
           , {{dbt_date.now()}}::time ppn_tm
           ,'{{ model.path }}' job_nm
           , {{ dbt_utils.generate_surrogate_key(  column_names )}} compare_key
     from source ar
  
)
--- final data
select   s.*        
  from source_rows s
