{#
    job name : txn_mini_dim
    mapping id : dm txn_mini_dim
    created by : tien.cm  
    modified by | date modify | description
    tien.cm     | 2023-09-04  | update code
#}
{{
    config(
        materialized = 'scd',
        unique_key = ['ccy_id','cnl_id'],
        identity_col = 'txn_mini_dim_id',
        strategy = 'type0',
        tags=["dim", "txn_mini_dim", "cl_val","cl_scm"]
    )
}}
--- declare all model in use
with 
ccy_lov as(
    select a.cl_id, a.cl_code
    from {{ref('cl_val')}} a
    join {{ref('cl_scm')}} b on a.cl_scm_id = b.cl_scm_id
    where b.scm_code = 'CCY-CODE'
),

cnl_lov as(
    select a.cl_id , a.cl_code
    from {{ref('cl_val')}} a
    join {{ref('cl_scm')}} b on a.cl_scm_id = b.cl_scm_id
    where b.scm_code =  'WAY4-TXN-CNL'
)

--- final data
select
         ccy_lov.cl_code alpb_ccy_code
        , ccy_lov.cl_id ccy_id
        , cnl_lov.cl_id cnl_id
        , cnl_lov.cl_code cnl_code
        , '{{ var("etl_date") }}'::timestamp as tf_created_at 
        , '{{model.path}}' job_nm
        , {{dbt_date.today()}}::date ppn_dt
        , {{dbt_date.now()}}::timestamp ppn_tm
from ccy_lov
cross join cnl_lov


