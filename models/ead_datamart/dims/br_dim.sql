{#
    job name : br_dim
    mapping ID : DM BR_DIM
    created by : tien.cm
    modified by | date modify | description
    tien.cm     | 2023-09-04  | update cdode
    nam.do      | 2023-10-30  | update stategy = type3
#}

{% set column_names = mcr_get_bussiness_columns(this, 'br_dim_id') %}
--- config job
{# nam.do add type 3: add type1_col #}
{{
    config(
        materialized = 'scd',
        unique_key=['br_id'],
        identity_col = 'br_dim_id',
        type1_col = ['br_nm'],
        pre_hook = scd2_revert('datamart', this),
        strategy = 'type3',
        full_refresh = false,
        tags=['dtm', 'dim', 'branch']
    )
}}

--- declare all model in use
with 
source as (

    select source.SRC_STM_ID  ,source.SRC_STM_CODE
    from {{ ref('src_stm')}}  source
    where source.SRC_STM_CODE =  'T24_VPB_COMPANY'
)

, ip as (
    select ip.IP_ID , ip.SRC_STM_ID , ip.UNQ_ID_IN_SRC_STM, ip.IP_NM
    from {{ ref('ip')}} ip
)

, temp as (
    select 
        ip.ip_id as br_id
        ,source.src_stm_code as src_stm
        ,ip.unq_id_in_src_stm
        ,ip.ip_nm br_nm
    from ip
    join source on ip.SRC_STM_ID  = source.SRC_STM_ID 

)
, source_rows as (
  
    select   {{'a.'~column_names| join(',a.')}}
           -------------technical columns-----------      
           , {{dbt_date.today()}} ppn_dt
           , {{dbt_date.now()}}::time ppn_tm
           , '{{ var("etl_date") }}'::timestamp as tf_created_at
           , '{{ var("end_date") }}'::timestamp as tf_updated_at
           , '{{ model.path }}' job_nm
           , {{ dbt_utils.generate_surrogate_key( column_names )}} compare_key
     from temp a
  
)
select  
       *
  from source_rows s