{#
    job name : card_ar_dim
    mapping ID : DM CARD_AR_DIM
    created by : nam.tran
    modified by | date modify | description
    tien.cm     | 2023-09-04  | update cdode
#}

{% set path = model.path %}
{% set column_names = mcr_get_bussiness_columns(this, 'card_ar_dim_id') %}
--- config job
{{
    config(
        materialized = 'scd',
        unique_key= ['card_ar_id', 'tf_created_at'],
        identity_col = 'card_ar_dim_id',
        strategy = 'type2',
        tags=['card'], 
        alias='model_ver_dim'
    )
}}
{# pre_hook = scd2_revert('ead_dtm', this),      #}
        
--- declare all model in use
with source as (

    select ar.card_ar_code card_ar_nbr
        ,ar.card_ar_id
        ,ar.src_stm
        ,ar.unq_id_in_src_stm::integer
        ,ar.eff_to_dt eff_mat_dt
        ,ar.lc_st
    from {{ ref('card_ar_smy')}}  ar
    where  snpst_dt = REPLACE('{{ var('etl_date') }}', '-', '')::INT

)
, source_rows as (
  
    select  {{'ar.'~column_names| join(',ar.')}}
           -------------technical columns-----------      
           , '{{ var("etl_date") }}'::date as tf_created_at
           , '{{ var("end_date") }}'::timestamp as tf_updated_at
           , {{dbt_date.today()}} ppn_dt
           , {{dbt_date.now()}}::time ppn_tm
           , {{ dbt_utils.generate_surrogate_key(  column_names )}} compare_key
     from source ar
  
)

--- process if is daily run
{% if is_incremental() %}
    , destination_rows as (---lay cac ban ghi co hieu luc o thoi diem hien tai    
        select *
               , {{ dbt_utils.generate_surrogate_key(  column_names)}} compare_key
         from {{ this }} 
        where tf_updated_at = cast('{{ var("end_date") }}' as timestamp)
    
    )

    , new_valid_to as (---xac dinh thong tin ban ghi moi duoc insert vao    
        select   {{'s.'~column_names| join(',s.')}}
                , s.ppn_dt
                , s.ppn_tm
                , s.tf_created_at
                , s.tf_updated_at  -- thong tin la thong tin moi nhat tu Source, hieu luc tu ngay Etl -> oo                         
          from source_rows s
          left join destination_rows d
            on s.card_ar_id = d.card_ar_id
         where nvl(s.compare_key, '$')  != nvl(d.compare_key, '$')      
    )

    , add_new_valid_to as (---End date ban ghi cu
        select   {{'d.'~column_names| join(',d.')}}         
                , n.ppn_dt
                , n.ppn_tm
                , d.tf_created_at
                , '{{ var("etl_date") }}'::timestamp as tf_updated_at  ---end date
          from destination_rows d
          join new_valid_to n
            on d.card_ar_id = n.card_ar_id
    )
    , not_exists_in_src as (  
        select  {{'d.'~column_names| join(',d.')}}
                ----------------
                ,{{dbt_date.today()}} ppn_dt
                ,{{dbt_date.now()}}::time ppn_tm
                , d.tf_created_at                 
                , '{{ var("etl_date") }}'::timestamp as tf_updated_at  ---end date                
          from destination_rows d
          left join source_rows n
            on d.card_ar_id = n.card_ar_id 
          where n.card_ar_id is null 
    )
    , final as (
        select n.*
        from add_new_valid_to n
        union all
        select o.* from new_valid_to o
        union all
        select d.* from not_exists_in_src d
    )
{% endif %}

--- final data
select   {{'s.'~column_names| join(',s.')}}
        --------------------------
        ,s.ppn_dt
        ,s.ppn_tm
        ,s.tf_created_at
        ,s.tf_updated_at
        ,'{{ model.path }}' job_nm
  from source_rows s
  --- process if is daily run
{% if is_incremental() %}
    Where 1=2 
    union all 
    Select 
    {{'f.'~column_names| join(',f.')}}
    --------------------------
    ,f.ppn_dt
    ,f.ppn_tm
    ,f.tf_created_at
    ,f.tf_updated_at
    ,'{{ model.path }}' job_nm 
    from final f
    
{% endif %}