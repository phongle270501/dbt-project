{#
    job name : card_dim
    mapping ID : DM CARD_DIM
    created by : tien.cm  
    modified by | date modify | description
    tien.cm     | 2023-09-04  | update code
#}
{% set column_names = mcr_get_bussiness_columns(this, 'card_dim_id') %}
{% set snpst_dt = var("etl_date")|replace('-','') %}
--- config job
{{
    config(
        materialized = 'scd',
        full_refresh = false,
        unique_key= ['card_id'],
        identity_col = 'card_dim_id',
        strategy = 'type2',
        pre_hook = scd2_revert('datamart', this),     
        tags=['dtm','dim', 'card_dim']
    )
}}

--- declare all model in use
with pymtc as (

    select  pymtc_code nbr
            ,pymtc_id card_id
            ,eff_fm_dt
            ,eff_to_dt
            ,src_stm 
            ,unq_id_in_src_stm
            ,last_actvn_dt
            , '{{ var("etl_date") }}'::timestamp as tf_created_at
    from {{ ref('card_smy') }}  
    where  snpst_dt = {{ snpst_dt }}
    
)
, source_rows as (
  
    select  {{'a.'~column_names| join(',a.')}}
            -------------technical columns-----------   
           , a.tf_created_at              
           , {{dbt_date.today()}} ppn_dt
           , {{dbt_date.now()}}::time ppn_tm
           , '{{ var("end_date") }}'::timestamp as tf_updated_at
           , '{{model.path}}' job_nm 
           , {{ dbt_utils.generate_surrogate_key( column_names )}} compare_key
     from pymtc a  
)
select   
        s.*
  from source_rows s
