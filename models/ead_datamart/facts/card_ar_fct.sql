{#
    job name : card_ar_fct
    mapping ID : DM CARD_AR_FCT
    created by : viet duc 
    modified by | date modify | description
    tien.cm     | 2023-09-04  | update cdode
#}
{% set _path = model.path %}
--- config job
{{
    config(
        materialized = 'incremental',
        pre_hook = """delete from {{this}} where cdr_dt  = REPLACE('{{ var('etl_date') }}', '-', '')::INT""",
        tags = ["dtm","fact", "card_ar_fct"]
    )
}}

with final as (
    select * from {{ ref('intr_dtm_card_ar_fct') }} 
)
select 
	s.CARD_AR_DIM_ID
    ,s.CDR_DT
    ,s.OPN_BR_DIM_ID
    ,s.CST_DIM_ID
    ,s.CLS_BAL_AMT
    ,s.ACR_INT_AMT
    ,s.NBR_ODUE_DYS
    ,s.ACR_INT_AMT_LAST_MO
    ,s.TOT_REPYMT_AMT_MTD
    ,'{{ _path }}' job_nm
    ,{{dbt_date.today()}} ppn_dt
    ,{{dbt_date.now()}}::time ppn_tm
from final s
