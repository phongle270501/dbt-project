{#
    job name : card_ar_fct
    mapping ID : DM CARD_AR_FCT
    created by : viet duc 
    modified by | date modify | description
    tien.cm     | 2023-09-04  | update cdode
#}

--- config job
{{
    config(
        materialized = 'incremental',
        pre_hook = """delete from {{this}} where CDR_DT  = REPLACE('{{ var('etl_date') }}', '-', '')::INT""",
        tags = ["dtm","fact","card_fct"]
    )
}}

select 
    s.CDR_DT
    ,s.CARD_DIM_ID
    ,s.CST_DIM_ID
    ,s.CARD_AR_DIM_ID
    ,s.OPN_BR_DIM_ID
    ,s.LMT_AMT
    ,s.DYS_SINCE_OPN
    ,{{dbt_date.today()}} ppn_dt
    ,{{dbt_date.now()}}::time ppn_tm
    ,'{{ model.path }}' job_nm
from {{ ref('intr_dtm_card_fct') }} s
