{#
    job name : card_ar_smy
    mapping ID : EAD_Redshift CARD_AR_SMY
    created by : tien.cm  
    modified by | date modify | description
    tien.cm     | 2023-09-04  | update cdode
#}

{% set snpst_dt = var("etl_date")|replace('-','') %}
--- config job
{{
    config(
        materialized = 'incremental',
        pre_hook = "delete from {{this}} where SNPST_DT  = "~ snpst_dt,
        tags=['smy', 'card_ar_smy']
    )
}}

with today as (
select 
    s.CARD_AR_ID
    ,cast(to_char(s.SNPST_DT,'YYYYMMDD') as integer) SNPST_DT
    ,s.CARD_AR_CODE
    ,s.EFF_FM_DT::date EFF_FM_DT
    ,s.EFF_TO_DT::date EFF_TO_DT
    ,s.SRC_STM
    ,s.UNQ_ID_IN_SRC_STM
    ,s.NBR_ODUE_DAY
    ,s.LC_ST
    ,s.LC_ST_NM
    ,s.TOT_REPYMT_AMT_MTD
    ,s.OPN_BR_ID
    ,s.OPN_BR_CODE
    ,s.CST_ID
    ,s.CST_CODE
    ,{{dbt_date.today()}} ppn_dt
    ,{{dbt_date.now()}}::time ppn_tm
    ,'{{ model.path }}' job_nm
from {{ ref('intr_awm_card_ar_smy') }} s
)

, preday as (
    select 
    CARD_AR_ID
    ,SNPST_DT
    ,CARD_AR_CODE
    ,EFF_FM_DT
    ,EFF_TO_DT
    ,SRC_STM
    ,UNQ_ID_IN_SRC_STM
    ,NBR_ODUE_DAY
    ,LC_ST
    ,LC_ST_NM
    ,TOT_REPYMT_AMT_MTD
    ,OPN_BR_ID
    ,OPN_BR_CODE
    ,CST_ID
    ,CST_CODE
    ,PPN_DT
    ,ppn_tm
    ,job_nm
    from {{this}} 
    where to_date(CAST(SNPST_DT as varchar),'YYYYMMDD') = '{{ var("etl_date") }}'::date - 1
)

, final as (
    select today.CARD_AR_ID
        , today.SNPST_DT
        , today.CARD_AR_CODE
        , today.EFF_FM_DT
        , today.EFF_TO_DT
        , today.SRC_STM
        , today.UNQ_ID_IN_SRC_STM
        , today.NBR_ODUE_DAY
        , today.LC_ST
        , today.LC_ST_NM
        , (nvl(today.TOT_REPYMT_AMT_MTD, 0) + nvl(preday.TOT_REPYMT_AMT_MTD, 0)) TOT_REPYMT_AMT_MTD
        , today.OPN_BR_ID
        , today.OPN_BR_CODE
        , today.CST_ID
        , today.CST_CODE
        , today.PPN_DT
        , today.ppn_tm
        , today.job_nm
    from today left join preday on today.CARD_AR_ID = preday.CARD_AR_ID
)
--- final data
select 
CARD_AR_ID
,SNPST_DT
,CARD_AR_CODE
,EFF_FM_DT
,EFF_TO_DT
,SRC_STM
,UNQ_ID_IN_SRC_STM
,NBR_ODUE_DAY
,LC_ST
,LC_ST_NM
,TOT_REPYMT_AMT_MTD
,OPN_BR_ID
,OPN_BR_CODE
,CST_ID
,CST_CODE
,PPN_DT
,ppn_tm
,job_nm 
from final
