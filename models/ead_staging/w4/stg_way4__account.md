{{ config(materialized='view') }}

with source as 
(
    select * from {{ source('w4', 'way4_account') }}
	where business_date =  to_char('{{ var("etl_date") }}'::date, 'yyyyMMdd')	
),


renamed as
(
    select 
	acnt_contract__oid	::	INTEGER			
	,code	::	VARCHAR(256)			
	,interest_rate	::	DOUBLE PRECISION		
	,id	::	INTEGER			
	,curr	::	INTEGER			
	,acat	::	VARCHAR(256)			
	,account_type	::	INTEGER			
	,is_am_available	::	VARCHAR(256)			
	,account_number	::	VARCHAR(256)			
	,acc_templ__id	::	INTEGER			
	,account_name	::	VARCHAR(256)			
	,cycle_date_to	::	DATE			
	,current_balance	::	DOUBLE PRECISION		
	,alter_account	::	VARCHAR(256)			
	,n_of_cycle	::	INTEGER			
	,gl_number	::	VARCHAR(256)			
	,top_account	::	INTEGER			
	,ageing_priority	::	INTEGER			
	,on_date	::	DATE			
	,on_date_balance	::	DOUBLE PRECISION		
	,business_date	::	INTEGER			
	,etl_dt	::	VARCHAR(256)			
	,act_dt	::	DATE			
	,act_key	::	VARCHAR(256)			
	,ppn_dt	::	DATE			
	,ppn_time	::	TIMESTAMP WITHOUT TIME ZONE

    from source
),

final as
(
    select
     	*
    from renamed
)

select * from final