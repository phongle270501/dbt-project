{{ config(materialized='view') }}

with source as 
(
    select * from {{ source('w4', 'way4_tbl_w4_acnt_contract') }}
    Where business_date =  to_char('{{ var("etl_date") }}'::date, 'yyyyMMdd')	
),

renamed as
(
    select 
        con_cat :: VARCHAR(256) 
        ,card_type :: VARCHAR(256) 
        ,source_info :: VARCHAR(256)   
        ,trim(SUBSTRING(unlock_date FROM 1 FOR 10))::date unlock_date 
        ,contract_number :: VARCHAR(256)   
        --,id :: INTEGER  
        ,trim(substring(id, 0, POSITION('.' IN id )))::INTEGER  as id
        ,company :: VARCHAR(256)   
        ,t24_acct_no :: VARCHAR(256)        
        ,production_status :: VARCHAR(256)
        ,date_open :: VARCHAR(256)   
        ,issued_type :: VARCHAR(256)   
        ,account_limit :: NUMERIC(32,5)  
        ,t24_al_id :: VARCHAR(256)   
        ,act_dt :: DATE  
        ,act_key :: VARCHAR(256)   
        ,ppn_dt :: DATE  
        ,ppn_time :: TIME
        ,amnd_date :: VARCHAR(256)   
        --,billing_day :: INTEGER  
        ,trim(substring(billing_day, 0, POSITION('.' IN billing_day )))::INTEGER  as billing_day 
        ,source_id :: VARCHAR(256)   
        --,acnt_contract__oid :: INTEGER  
        ,trim(substring(acnt_contract__oid, 0, POSITION('.' IN acnt_contract__oid )))::INTEGER  as acnt_contract__oid 
        ,ecom_status :: VARCHAR(256)   
        ,product_group :: VARCHAR(256)   
        ,contr_status :: VARCHAR(256)   
        --,client_id :: INTEGER 
        ,trim(substring(client_id, 0, POSITION('.' IN client_id )))::INTEGER  as client_id 
        ,bad_debt_date :: VARCHAR(256)   
        ,card_expire :: INTEGER  
        ,date_expire :: VARCHAR(256)   
        ,business_date :: INTEGER  
        ,add_info_01 :: VARCHAR(256)   
        ,cert_id :: VARCHAR(256)   
        ,unlock_info :: VARCHAR(256)   
        ,add_info_02 :: VARCHAR(256)   
    from source
),

final as
(
    select
     	*
    from renamed
)

select * from final