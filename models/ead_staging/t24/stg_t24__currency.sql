{{ config(materialized='view') }}

with source as 
(
    select * from {{ source('t24', 't24_currency') }} 
	--where t24_load_date = to_char('{{ var("etl_date") }}'::date, 'dd-Mon-YYYY')	
),


renamed as
(
    select 
		currency_code :: VARCHAR(256),
		rank :: DECIMAL(32,5),
		numeric_ccy_code :: DECIMAL(32,5),
		no_of_decimals :: DECIMAL(32,5),
		quotation_code :: DECIMAL(32,5),
		quotation_pips :: DECIMAL(32,5),
		days_delivery :: DECIMAL(32,5),
		days_forward :: DECIMAL(32,5),
		interest_day_basis :: VARCHAR(256),
		rate_allowance :: VARCHAR(256),
		fixing_date :: VARCHAR(256),
		min_round_amount :: VARCHAR(256),
		cash_only_rounding :: VARCHAR(256),
		min_round_type :: DECIMAL(32,5),
		cash_round_type :: DECIMAL(32,5),
		max_rate :: DECIMAL(32,5),
		spread_sell :: DECIMAL(32,5),
		seriestg :: VARCHAR(256),
		spread_buy :: DECIMAL(32,5),
		diem_chenh_mua :: DECIMAL(32,5),
		diem_chenh_ban :: DECIMAL(32,5),
		swift_code :: VARCHAR(256),
		precious_metal :: VARCHAR(256),
		country_code :: VARCHAR(256),
		fixed_rate :: VARCHAR(256),
		fixed_ccy :: VARCHAR(256),
		fixed_start_date :: VARCHAR(256),
		base_ccy_rank :: DECIMAL(32,5),
		available_date :: VARCHAR(256),
		blocked_date :: VARCHAR(256),
		cut_off_time :: VARCHAR(256),
		cls_ccy :: VARCHAR(256),
		record_status :: VARCHAR(256),
		curr_no :: DECIMAL(32,5),
		authoriser :: VARCHAR(256),
		co_code :: VARCHAR(256),
		dept_code :: DECIMAL(32,5),
		auditor_code :: VARCHAR(256),
		audit_date_time :: VARCHAR(256),
		efz_co_code :: VARCHAR(256),
		act_dt :: date,
		act_key :: CHAR(1),
		ppn_dt :: date,
		ppn_time :: time
    from source
),

final as
(
    select
     	*
    from renamed
)

select * from final


