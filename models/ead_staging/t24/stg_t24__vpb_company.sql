with source as 
(
    select * from {{ source('t24', 't24_vpb_company') }}
	where t24_load_date = to_char('{{ var("etl_date") }}'::date, 'dd-Mon-YYYY')	
),


renamed as
(
    select 
		id	::	varchar(256)	,
		main_branch_code	::	varchar(256)	,
		segment	::	varchar(256)	,
		map_sbv	::	decimal(32,5)	,
		region_sme_code	::	varchar(256)	,
		region_sme_name	::	varchar(256)	,
		region_khcn_code	::	varchar(256)	,
		region_khcn_name	::	varchar(256)	,
		region	::	varchar(256)	,
		province	::	varchar(256)	,
		branch_type	::	varchar(256)	,
		from_date	::	decimal(32,5)	,
		to_date	::	decimal(32,5)	,
		status	::	varchar(256)	,
		co_code	::	varchar(256)	,
		act_dt	::	date	,
		act_key	::	varchar(256)	,
		ppn_dt	::	date	,
		ppn_time	::	time	,
		special_book	::	varchar(256)	,
		opening_date	::	decimal(32,5)	,
		close_date	::	varchar(256)	,
		director_name	::	varchar(256)	,
		deputy_director_name	::	varchar(256)	,
		teller_name	::	varchar(256)	,
		staff_escort_name	::	varchar(256)	,
		record_status	::	varchar(256)	,
		curr_no	::	decimal(32,5)	,
		authoriser	::	varchar(256)	,
		dept_code	::	decimal(32,5)	,
		auditor_code	::	varchar(256)	,
		audit_date_time	::	varchar(256)	,
		t24_load_date	::	date	,
		efz_load_date	::	date	,
		efz_co_code	::	varchar(256)	,
		code_province	::	decimal(32,5)	
    from source
),

final as
(
    select
     	*
    from renamed
)

select * from final


