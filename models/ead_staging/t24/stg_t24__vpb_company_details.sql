{{ config(materialized='view') }}

with source as 
(
    select * from {{ source('t24', 't24_vpb_company_details') }}
	where t24_load_date = to_char('{{ var("etl_date") }}'::date, 'dd-Mon-YYYY')	
),


renamed as
(
    select 
		id	::	varchar(256)	,
		m	::	decimal(32,5)	,
		s	::	decimal(32,5)	,
		branch_name	::	varchar(256)	,
		address	::	varchar(256)	,
		act_dt	::	date	,
		act_key	::	varchar(256)	,
		ppn_dt	::	date	,
		ppn_time	::	time ,
		efz_co_code	::	varchar(256)	,
		t24_load_date	::	date	,
		efz_load_date	::	date	,
		notes	::	varchar(256)	,
		inputter	::	varchar(256)	,
		date_time	::	bigint	,
		co_code	::	varchar(256)	
    from source
),

final as
(
    select
     	*
    from renamed
)

select * from final


