#!/bin/bash
echo "------------------ Test DBT ------------------"
process_string() {
  local str="$1"

  # Remove 'intr_awm_' or 'intr_dtm_' from the start, if they exist
  str="${str#intr_awm_}"
  str="${str#intr_dtm_}"

  # Remove everything after and including '__'
  str="${str%%__*}"

  echo "$str"
}

for stringPath in $(git diff --name-only $CI_COMMIT_BEFORE_SHA $CI_COMMIT_SHA)
do
  if [[ $stringPath == *"models/"* ]]; then
    filename_with_extension="${stringPath##*/}"
    filename="${filename_with_extension%.*}"
    echo "###Test job:$filename"

    tablename=$(process_string "$filename")
    echo "###Run test refer table: $tablename"
    dbt test --select tag:$tablename
  fi
done
